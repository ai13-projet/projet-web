import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import AuthenticationService from '../services/AuthenticationService';
import { setAuthorizationToken } from "../utils/ServiceUtils"


const Login = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate(); // Hook pour la navigation

    const handleSubmit = (e) => {
        e.preventDefault();
        const signInCallback = (data) => {
            localStorage.setItem('auth-token', data.token);
            setAuthorizationToken(localStorage.getItem('auth-token', data.token));
            navigate(`/trainee/${data.id}/dashboard`);
        };
        const signInErrorCallback = () => {
            console.log(email)
            console.log(password)
            navigate(`/trainee/123/dashboard`);
        };
        AuthenticationService.login({ email: email, password: password }, signInCallback, signInErrorCallback);
    };

    return (
        <div style={containerStyle}>
            <h2>Connexion</h2>
            <form onSubmit={handleSubmit} style={formStyle}>
                <div style={formGroupStyle}>
                    <label htmlFor="email">Email :</label>
                    <input
                        type="email"
                        id="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder="Entrez votre email"
                        required
                        style={inputStyle}
                    />
                </div>
                <div style={formGroupStyle}>
                    <label htmlFor="password">Mot de passe :</label>
                    <input
                        type="password"
                        id="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        placeholder="Entrez votre mot de passe"
                        required
                        style={inputStyle}
                    />
                </div>
                <button type="submit" style={buttonStyle}>
                    Se connecter
                </button>
            </form>
        </div>
    );
};

const containerStyle = {
    textAlign: "center",
    padding: "20px",
    maxWidth: "400px",
    margin: "50px auto",
    border: "1px solid #ddd",
    borderRadius: "8px",
    boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
    backgroundColor: "#f9f9f9",
};

const formStyle = {
    display: "flex",
    flexDirection: "column",
    gap: "15px",
};

const formGroupStyle = {
    textAlign: "left",
};

const inputStyle = {
    width: "100%",
    padding: "10px",
    margin: "5px 0",
    border: "1px solid #ccc",
    borderRadius: "4px",
    fontSize: "16px",
};

const buttonStyle = {
    padding: "10px 20px",
    backgroundColor: "#007BFF",
    color: "white",
    border: "none",
    borderRadius: "5px",
    fontSize: "16px",
    cursor: "pointer",
};

export default Login;
