import React from "react";
import { Link } from "react-router-dom";

const Home = () => {
    localStorage.setItem('auth-token', null);
    return (
        <div style={{textAlign: "center", padding: "20px"}}>
            <h1>Bienvenue dans l'application des stagiaires</h1>
            <p>
                Connectez-vous pour accéder à votre tableau de bord et commencer
                l'évaluation.
            </p>
            <div style={{marginTop: "20px"}}>
                <Link to="/login" style={linkStyle}>
                    Se connecter
                </Link>
            </div>
            <div style={{marginTop: "20px"}}>
                <Link to="/visitor/dashboard" style={linkStyle}>
                    visiter
                </Link>
            </div>
        </div>
    );
};

const linkStyle = {
    display: "inline-block",
    padding: "10px 20px",
    margin: "10px",
    backgroundColor: "#007BFF",
    color: "white",
    textDecoration: "none",
    borderRadius: "5px",
    fontSize: "16px",
};

export default Home;
