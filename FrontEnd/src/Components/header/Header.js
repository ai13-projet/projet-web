import React from 'react';
import  BackToHomeButton  from './BackToHomeButton';
import  LogoutButton  from './LogoutButton';
import  BackToDashboardButton  from './BackToDashboardButton';
import { useLocation } from "react-router-dom";

const Header = ({ title }) => {
    const location = useLocation();

    // Détermine le type de bouton en fonction de l'URL
    const getButtonInfo = () => {
        const path = location.pathname;
        const userType = path.split("/")[1]; // Récupère le premier segment de l'URL

        if (userType === "visitor") {
            if (path.includes("/dashboard")) {
                return { label: "Acceuil", component: <BackToHomeButton />,};
            } else {
                return { label: "Dashboard", component: <BackToDashboardButton /> };
            }
        }else if (path.includes("/dashboard")) {
                return { label: "Se déconnecter", component: <LogoutButton /> };
            } else {
                return { label: "Dashboard", component: <BackToDashboardButton />  };
            }
    };

    const { label, component } = getButtonInfo();

    return (
        <header style={headerStyle}>
            <h1 style={titleStyle}>{title}</h1>
            <div style={buttonContainerStyle}>
                {component}
            </div>
        </header>
    );
};


const headerStyle = {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "10px 20px",
    backgroundColor: "#007BFF",
    color: "white",
    boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
};

const titleStyle = {
    margin: 0,
    fontSize: "24px",
};

const buttonContainerStyle = {
    display: "flex",
    alignItems: "center",
    gap: "10px",
};

const labelStyle = {
    fontSize: "16px",
    color: "white",
};

export default Header;
