import React from "react";
import { useNavigate, useLocation } from "react-router-dom";
import { useParams } from 'react-router-dom';

const BackToDashboardButton = () => {
    const navigate = useNavigate();
    const location = useLocation();
    const params = useParams();

    // Récupère le premier segment de l'URL après le "/"
    const userType = location.pathname.split("/")[1];

    const handleBackClick = () => {
        if(userType == "trainee"){
            navigate(`/${userType}/${params.traineeId}/dashboard`); // Navigue vers le dashboard du type d'utilisateur
        } else {
            navigate(`/${userType}/dashboard`); // Navigue vers le dashboard du type d'utilisateur
        }

    };

    return (
        <button onClick={handleBackClick} style={buttonStyle}>
            Retour au Dashboard
        </button>
    );
};

const buttonStyle = {
    padding: "12px 24px",
    backgroundColor: "#041866", // Une couleur orange vif
    color: "white",
    border: "none",
    borderRadius: "25px", // Bord arrondi pour un effet moderne
    fontSize: "16px",
    fontWeight: "bold",
    boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.2)", // Ombre pour le faire ressortir
    cursor: "pointer",
    transition: "all 0.3s ease", // Effet de transition pour le survol
};

export default BackToDashboardButton;
