import React from "react";
import { useNavigate, useLocation } from "react-router-dom";


const LogOutButton = () => {
  const navigate = useNavigate();
  const location = useLocation();

  const handleBackClick = () => {
    navigate(`/`); // Navigue vers le dashboard du type d'utilisateur
  };

  return (
      <button onClick={handleBackClick} style={buttonStyle}>
        Log out
      </button>
  );
};

const buttonStyle = {
  padding: "12px 24px",
  backgroundColor: "#041866", // Une couleur orange vif
  color: "white",
  border: "none",
  borderRadius: "25px", // Bord arrondi pour un effet moderne
  fontSize: "16px",
  fontWeight: "bold",
  boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.2)", // Ombre pour le faire ressortir
  cursor: "pointer",
  transition: "all 0.3s ease", // Effet de transition pour le survol
};

export default LogOutButton;
