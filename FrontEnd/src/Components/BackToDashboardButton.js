import React from "react";
import { useNavigate } from "react-router-dom";

const BackToDashboardButton = () => {
    const navigate = useNavigate();

    const handleBackClick = () => {
        navigate("/dashboard");
    };

    return (
        <button onClick={handleBackClick} style={buttonStyle}>
            Retour au Dashboard
        </button>
    );
};

const buttonStyle = {
    padding: "10px 20px",
    margin: "20px 0",
    backgroundColor: "#007BFF",
    color: "white",
    border: "none",
    borderRadius: "5px",
    fontSize: "16px",
    cursor: "pointer",
};

export default BackToDashboardButton;
