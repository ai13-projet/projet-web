import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Header from "../header/Header";
import BackToDashboardButton from "../header/BackToDashboardButton";
import QuizService from "../../services/QuizService";

const QuestionnaireList = () => {
    const [quizzes, setQuizzes] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [quizzesPerPage] = useState(5);
    const params = useParams();

    // Récupérer les quizzes depuis l'API
    useEffect(() => {
        const fetchQuizzes = async () => {
            try {
                QuizService.getActiveQuizzes(
                    (response) => {
                        if (response.data) {
                            setQuizzes(response.data); // Accéder à data.quizzes
                        }
                    },
                    (error) => console.error("Erreur lors de la récupération des quizzes:", error)
                );
            } catch (err) {
                console.error("Erreur de récupération des données:", err);
            }
        };
        fetchQuizzes();
    }, []);

    // Pagination des quizzes
    const indexOfLastQuiz = currentPage * quizzesPerPage;
    const indexOfFirstQuiz = indexOfLastQuiz - quizzesPerPage;
    const currentQuizzes = quizzes.slice(indexOfFirstQuiz, indexOfLastQuiz);
    const totalPages = Math.ceil(quizzes.length / quizzesPerPage);

    const handlePageChange = (pageNumber) => {
        setCurrentPage(pageNumber);
    };

    return (
        <div style={containerStyle}>
            <Header title="Liste des Quizzes" />
            <ul style={listStyle}>
                {currentQuizzes.map((quiz) => (
                    <li key={quiz.id} style={itemStyle}>
                        <h3>{quiz.title}</h3>
                        <p><strong>ID du Quiz :</strong> {quiz.id}</p>
                        <p><strong>Créateur :</strong> {quiz.creatorId}</p>
                        <p><strong>Thème :</strong> {quiz.themeLabel}</p>
                        <p><strong>Statut :</strong> {quiz.active ? "Actif" : "Inactif"}</p>
                        <a href={`/trainee/${params.traineeId}/quizz/${quiz.id}`} style={linkStyle}>
                            Démarrer ce quiz
                        </a>
                    </li>
                ))}
            </ul>
            <div style={paginationStyle}>
                {Array.from({ length: totalPages }, (_, index) => (
                    <button
                        key={index}
                        onClick={() => handlePageChange(index + 1)}
                        style={{
                            ...buttonStyle,
                            backgroundColor: currentPage === index + 1 ? "#007BFF" : "#f0f0f0",
                            color: currentPage === index + 1 ? "white" : "black",
                        }}
                    >
                        {index + 1}
                    </button>
                ))}
            </div>
        </div>
    );
};

// Styles CSS en JS
const containerStyle = {
    textAlign: "center",
    padding: "20px",
    maxWidth: "800px",
    margin: "50px auto",
};

const listStyle = {
    listStyle: "none",
    padding: 0,
};

const itemStyle = {
    marginBottom: "20px",
    padding: "15px",
    border: "1px solid #ddd",
    borderRadius: "8px",
    textAlign: "left",
    backgroundColor: "#f9f9f9",
};

const linkStyle = {
    display: "inline-block",
    padding: "10px 20px",
    marginTop: "10px",
    backgroundColor: "#007BFF",
    color: "white",
    textDecoration: "none",
    borderRadius: "5px",
    fontSize: "14px",
};

const paginationStyle = {
    marginTop: "20px",
    display: "flex",
    justifyContent: "center",
    gap: "10px",
};

const buttonStyle = {
    padding: "10px 15px",
    border: "none",
    borderRadius: "5px",
    cursor: "pointer",
    fontSize: "14px",
};

export default QuestionnaireList;
