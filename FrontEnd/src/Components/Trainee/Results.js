import React, { useEffect, useState } from "react";
import BackToDashboardButton from "../header/BackToDashboardButton";
import Header from "../header/Header";
import { useParams } from "react-router-dom";
import UserService from "../../services/UserService";

const Results = () => {
    const [stats, setStats] = useState({
        averageScore: 0,
        averageTime: 0,
        numberOfQuizzes: 0,
        rankings: [],
    });
    const [error, setError] = useState(false);
    const [currentPage, setCurrentPage] = useState(1);
    const [resultsPerPage] = useState(2); // Nombre de résultats par page

    const params = useParams();

    // Récupérer les statistiques depuis l'API
    useEffect(() => {
        UserService.getUserStatistics(
            parseInt(params.traineeId),
            (data) => {
                if (data) {
                    //JSON.parse(data)
                    console.log(data)
                    // Mise à jour des stats globales et des rankings
                    setStats({
                        averageScore: data.averageScore,
                        averageTime: data.averageTime,
                        numberOfQuizzes: data.numberOfQuizzes,
                        rankings: data.rankings || [],
                    });
                }
            },
            (error) => {
                console.error("Erreur lors de la récupération des statistiques :", error);
                setError(true);
            }
        );
    }, [params.userId]);

    // Pagination logic
    const indexOfLastResult = currentPage * resultsPerPage;
    const indexOfFirstResult = indexOfLastResult - resultsPerPage;
    const currentRankings = stats.rankings.slice(indexOfFirstResult, indexOfLastResult);
    const totalPages = Math.ceil(stats.rankings.length / resultsPerPage);

    const handlePageChange = (pageNumber) => {
        setCurrentPage(pageNumber);
    };

    if (error) {
        return <p style={errorStyle}>Une erreur est survenue lors de la récupération des statistiques.</p>;
    }

    return (
        <div style={containerStyle}>
            <Header title="Statistiques de l'utilisateur" />

            {/* Statistiques globales */}
            <div style={globalStatsStyle}>
                <h2>Statistiques Globales</h2>
                <p>
                    <strong>Score moyen :</strong> {stats.averageScore}
                </p>
                <p>
                    <strong>Temps moyen :</strong> {stats.averageTime} secondes
                </p>
                <p>
                    <strong>Nombre de quiz complétés :</strong> {stats.numberOfQuizzes}
                </p>
            </div>

            {/* Statistiques par quiz avec pagination */}
            <div>
                <h2 style={{ marginTop: "20px" }}>Statistiques par Quiz</h2>
                {currentRankings.length > 0 ? (
                    <ul style={listStyle}>
                        {currentRankings.map((ranking, index) => (
                            <li key={index} style={itemStyle}>
                                <p>
                                    <strong>Titre du Quiz :</strong> {ranking.quizTitle}
                                </p>
                                <p>
                                    <strong>Rang :</strong> {ranking.rank}
                                </p>
                                <p>
                                    <strong>Score Ratio :</strong> {ranking.scoreRatio}
                                </p>
                            </li>
                        ))}
                    </ul>
                ) : (
                    <p>Aucune statistique disponible pour les quiz.</p>
                )}

                {/* Pagination */}
                <div style={paginationStyle}>
                    {Array.from({ length: totalPages }, (_, index) => (
                        <button
                            key={index}
                            onClick={() => handlePageChange(index + 1)}
                            style={{
                                ...paginationButtonStyle,
                                backgroundColor: currentPage === index + 1 ? "#007BFF" : "#f0f0f0",
                                color: currentPage === index + 1 ? "white" : "black",
                            }}
                        >
                            {index + 1}
                        </button>
                    ))}
                </div>
            </div>
        </div>
    );
};

const containerStyle = {
    textAlign: "center",
    padding: "20px",
    maxWidth: "800px",
    margin: "50px auto",
    backgroundColor: "#f9f9f9",
    borderRadius: "8px",
    boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
};

const globalStatsStyle = {
    marginBottom: "30px",
    padding: "15px",
    border: "1px solid #ddd",
    borderRadius: "8px",
    backgroundColor: "#e0f7fa",
    textAlign: "left",
    boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)",
};

const listStyle = {
    listStyle: "none",
    padding: 0,
};

const itemStyle = {
    marginBottom: "20px",
    padding: "15px",
    border: "1px solid #ddd",
    borderRadius: "8px",
    textAlign: "left",
    backgroundColor: "#ffffff",
    boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)",
};

const paginationStyle = {
    display: "flex",
    justifyContent: "center",
    marginTop: "20px",
    gap: "10px",
};

const paginationButtonStyle = {
    padding: "10px",
    border: "none",
    borderRadius: "5px",
    cursor: "pointer",
    fontSize: "14px",
};

const errorStyle = {
    color: "red",
    textAlign: "center",
    marginTop: "20px",
};

export default Results;
