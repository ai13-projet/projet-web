import { Container, Stack, Typography, Box, Alert, AlertTitle, LinearProgress } from "@mui/material";
import { useState, useEffect, useCallback, useMemo } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Header from "../header/Header";
import RecordService from "../../services/RecordService";
import QuestionService from "../../services/QuestionService";

const QuestionnaireActif = () => {
    const [traineeAnswers, setTraineeAnswers] = useState([]);
    const [alreadyAskedQuestions, setAlreadyAskedQuestions] = useState([]);
    const [currentQuestion, setCurrentQuestion] = useState(null);
    const [count, setCount] = useState(1);
    const [score, setScore] = useState(0);
    const [quiz, setQuiz] = useState({ questions: [] });
    const [error, setError] = useState(false);
    const [isLoaded, setIsLoaded] = useState(false);
    const [recordId, setRecordId] = useState(null); // Stocker l'ID du record


    const params = useParams();
    const navigate = useNavigate();

    // Créer un record et récupérer les questions associées au quiz
    useEffect(() => {
        const startRecordAndFetchQuestions = () => {
            RecordService.createRecord(
                parseInt(params.traineeId),
                parseInt(params.quizzId),
                {},
                (recordResponse) => {
                    console.log("Record créé :", recordResponse);
                    setRecordId(recordResponse.record.id); // Stocker l'ID du record

                    // Récupérer les questions
                    QuestionService.getQuestionsByQuiz(
                        parseInt(params.quizzId),
                        (questionResponse) => {
                            console.log("Questions récupérées :", questionResponse.data);
                            setQuiz({ questions: questionResponse.data });
                            setCurrentQuestion(questionResponse.data[0]); // Première question
                            setIsLoaded(true);
                        },
                        (error) => {
                            console.error("Erreur lors de la récupération des questions :", error);
                            setError(true);
                        }
                    );
                },
                (error) => {
                    console.error("Erreur lors de la création du record :", error);
                    setError(true);
                }
            );
        };

        startRecordAndFetchQuestions();
    }, [params.traineeId, params.quizzId]);

    // Obtenir la question suivante
    const getNextQuestion = (currentId) => {
        const nextIndex = quiz.questions.findIndex((q) => q.id === currentId) + 1;
        return nextIndex < quiz.questions.length ? quiz.questions[nextIndex] : null;
    };

    // Gérer le clic sur une réponse
    const handleAnswerClick = useCallback(
        (e, answer) => {
            const isCorrect = answer.correct;

            // Envoyer la réponse à l'API
            if (recordId) {
                const answerData = [{ questionId: currentQuestion.id, answerId: answer.id }];
                RecordService.addAnswersToRecord(
                    recordId,
                    answerData,
                    (response) => {
                        console.log("Réponse ajoutée avec succès :", response);
                    },
                    (error) => {
                        console.error("Erreur lors de l'ajout de la réponse :", error);
                    }
                );
            }

            // Mise à jour des états locaux
            setTraineeAnswers((prev) => [...prev, { questionId: currentQuestion.id, answerId: answer.id }]);
            setAlreadyAskedQuestions((prev) => [...prev, currentQuestion.id]);
            setScore((prev) => (isCorrect ? prev + 1 : prev));

            e.target.classList.add(isCorrect ? "answer-true" : "answer-false");

            setTimeout(() => {
                const nextQuestion = getNextQuestion(currentQuestion.id);
                setCurrentQuestion(nextQuestion);
                setCount((prev) => prev + 1);
            }, 1000);
        },
        [currentQuestion, quiz.questions, recordId]
    );

    // Détecter si toutes les questions ont été répondues
    const recordData = useMemo(
        () => ({
            score,
            quizId: params.quizzId,
            traineeId: params.traineeId,
            answers: traineeAnswers,
        }),
        [score, params.quizzId, params.traineeId, traineeAnswers]
    );

    useEffect(() => {
        if (traineeAnswers.length === quiz.questions.length && quiz.questions.length > 0) {
            console.log("Enregistrement des résultats :", recordData);
            navigate(`/trainee/${params.traineeId}/results/${params.quizzId}`, {
                state: { score, total: quiz.questions.length },
            });
        }
    }, [traineeAnswers, quiz.questions.length, recordData, navigate]);

    if (error) {
        return (
            <Alert severity="error">
                <AlertTitle>Erreur</AlertTitle>
                Impossible de charger les données du quiz.
            </Alert>
        );
    }

    if (!isLoaded) {
        return <LinearProgress />;
    }

    return (
        <Container maxWidth="md" sx={{ marginTop: "20px" }}>
            <Header title={`Quiz ${params.quizzId}`} />
            <Stack spacing={2} sx={{ marginTop: "20px" }}>
                {currentQuestion ? (
                    <>
                        <Typography variant="h6" sx={{ textAlign: "center", fontWeight: "bold" }}>
                            Question {count} / {quiz.questions.length}
                        </Typography>
                        <Box>
                            <Typography variant="h5" sx={{ fontWeight: "bold", textAlign: "center", color: "#333" }}>
                                {currentQuestion.label}
                            </Typography>
                        </Box>
                        <Stack spacing={2}>
                            {currentQuestion.answers.map((answer) => (
                                <Box
                                    key={answer.id}
                                    className="answer"
                                    onClick={(e) => handleAnswerClick(e, answer)}
                                    sx={{
                                        backgroundColor: "#4A90E2",
                                        color: "#fff",
                                        borderRadius: "10px",
                                        padding: "10px 20px",
                                        textAlign: "center",
                                        cursor: "pointer",
                                        boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)",
                                        "&:hover": {
                                            backgroundColor: "#3b7ed0",
                                            transform: "scale(1.02)",
                                        },
                                    }}
                                >
                                    <Typography variant="body1" sx={{ fontWeight: "bold" }}>
                                        {answer.label}
                                    </Typography>
                                </Box>
                            ))}
                        </Stack>
                    </>
                ) : (
                    <Typography variant="h5" sx={{ textAlign: "center", fontWeight: "bold", color: "#4A90E2" }}>
                        Merci d'avoir terminé le quiz !
                    </Typography>
                )}
            </Stack>
        </Container>
    );
};

export default QuestionnaireActif;