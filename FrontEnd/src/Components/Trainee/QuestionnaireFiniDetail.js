import { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { Alert, AlertTitle, Container, LinearProgress, Stack, Typography, Box, Button } from "@mui/material";
import Header from "../header/Header";

const QuestionnaireFiniDetail = () => {
    const params = useParams();
    const navigate = useNavigate();
    const [record, setRecord] = useState(null);
    const [error, setError] = useState(false);
    const [isLoaded, setIsLoaded] = useState(false);
    const [activeQuestions, setActiveQuestions] = useState([]);
    const isAdmin = JSON.parse(localStorage.getItem("isAdmin") || "false");

    useEffect(() => {
        // Simuler un appel API pour récupérer les résultats détaillés
        const fetchRecord = async () => {
            try {
                // Simuler des données fictives
                const mockRecord = {
                    id: params.recordId,
                    user: { id: 1, name: "Utilisateur Exemple" },
                    quiz: {
                        label: "Exemple de Quiz",
                        questions: [
                            {
                                id: 1,
                                label: "Quelle est la capitale de la France ?",
                                answers: [
                                    { id: 1, label: "Paris", correct: true },
                                    { id: 2, label: "Londres", correct: false },
                                    { id: 3, label: "Berlin", correct: false },
                                ],
                                active: true,
                            },
                            {
                                id: 2,
                                label: "Quelle est la capitale de l'Allemagne ?",
                                answers: [
                                    { id: 4, label: "Madrid", correct: false },
                                    { id: 5, label: "Berlin", correct: true },
                                    { id: 6, label: "Rome", correct: false },
                                ],
                                active: true,
                            },
                        ],
                    },
                    answers: [
                        { questionId: 1, answerId: 1 },
                        { questionId: 2, answerId: 5 },
                    ],
                };

                setTimeout(() => {
                    setRecord(mockRecord);
                    setActiveQuestions(mockRecord.quiz.questions.filter((q) => q.active));
                    setIsLoaded(true);
                }, 500);
            } catch {
                setError(true);
                setIsLoaded(true);
            }
        };

        fetchRecord();
    }, [params.recordId]);

    const handleBackNavigation = () => {
        if (isAdmin) {
            navigate(`/admin/user/${record.user.id}`);
        } else {
            navigate(`/trainee/${record.user.id}/quizzes`);
        }
    };

    if (error) {
        return (
            <Alert severity="error">
                <AlertTitle>Erreur</AlertTitle>
                Impossible de charger les données détaillées.
            </Alert>
        );
    }

    if (!isLoaded) {
        return <LinearProgress />;
    }

    return (
        <Container maxWidth="md" sx={{ marginTop: "20px" }}>
            <Header title="Résultats Détaillés" />
            <Stack spacing={3} sx={{ marginTop: "20px" }}>
                <Box
                    sx={{
                        backgroundColor: "#f9f9f9",
                        padding: "20px",
                        borderRadius: "10px",
                        boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)",
                    }}
                >
                    <Typography variant="h5" sx={{ fontWeight: "bold", textAlign: "center" }}>
                        {record.quiz.label}
                    </Typography>
                    <Typography variant="body1" sx={{ textAlign: "center", marginTop: "10px" }}>
                        Résumé des réponses
                    </Typography>
                </Box>

                <Typography
                    variant="h6"
                    sx={{ textAlign: "center", fontWeight: "bold", color: "#4A90E2", marginTop: "20px" }}
                >
                    {isAdmin ? "Réponses de l'utilisateur" : "Vos réponses"}
                </Typography>

                <Stack direction="column" spacing={2}>
                    {activeQuestions.map((question) => (
                        <Box
                            key={question.id}
                            sx={{
                                backgroundColor: "#fff",
                                padding: "15px",
                                borderRadius: "10px",
                                boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)",
                            }}
                        >
                            <Typography variant="body1" sx={{ fontWeight: "bold", marginBottom: "10px" }}>
                                {question.label}
                            </Typography>
                            {question.answers.map((answer) => {
                                const isSelected =
                                    record.answers.find((a) => a.questionId === question.id)?.answerId === answer.id;
                                return (
                                    <Box
                                        key={answer.id}
                                        sx={{
                                            backgroundColor: isSelected
                                                ? answer.correct
                                                    ? "#4caf50" // Vert si correct
                                                    : "#f44336" // Rouge si incorrect
                                                : "#f1f1f1",
                                            padding: "10px",
                                            borderRadius: "5px",
                                            marginBottom: "5px",
                                            boxShadow: "0 1px 2px rgba(0, 0, 0, 0.1)",
                                        }}
                                    >
                                        <Typography variant="body2" sx={{ color: isSelected ? "#fff" : "#333" }}>
                                            {answer.label}
                                        </Typography>
                                    </Box>
                                );
                            })}
                        </Box>
                    ))}
                </Stack>
            </Stack>
            <Stack alignItems="center" mt={3}>
                <Button
                    variant="outlined"
                    sx={{
                        color: "#4A90E2",
                        borderColor: "#4A90E2",
                        borderRadius: "20px",
                        textTransform: "none",
                        padding: "10px 20px",
                        "&:hover": { backgroundColor: "#f0f0f0", borderColor: "#4A90E2" },
                    }}
                    onClick={handleBackNavigation}
                >
                    Retour
                </Button>
            </Stack>
        </Container>
    );
};

export default QuestionnaireFiniDetail;
