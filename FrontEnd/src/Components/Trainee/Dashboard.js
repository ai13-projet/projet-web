import React from "react";
import { Link } from "react-router-dom";
import LogoutButton from "../header/LogoutButton";
import Header from "../header/Header";
import { useParams } from 'react-router-dom';


const Dashboard = () => {
    const params = useParams();


    return (
        <div style={containerStyle}>
            <Header title="Bienvenue sur le Dashboard" />
            <p>
                Vous pouvez accéder aux fonctionnalités suivantes :
            </p>
            <div style={menuStyle}>
                <Link to={"/trainee/" + params.traineeId +"/questionnaires"} style={linkStyle}>
                    📋 Liste des Questionnaires
                </Link>
                <Link to={"/trainee/" + params.traineeId + "/results"} style={linkStyle}>
                    📊 Résultats d'Évaluation
                </Link>
                <Link to={"/trainee/" + params.traineeId + "/profile"} style={linkStyle}>
                    👤 Gérer mon Profil
                </Link>
            </div>
        </div>
    );
};

const containerStyle = {
    textAlign: "center",
    padding: "20px",
    maxWidth: "600px",
    margin: "50px auto",
    backgroundColor: "#f9f9f9",
    borderRadius: "8px",
    boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
};

const menuStyle = {
    display: "flex",
    flexDirection: "column",
    gap: "15px",
    marginTop: "20px",
};

const linkStyle = {
    display: "block",
    padding: "10px 20px",
    backgroundColor: "#007BFF",
    color: "white",
    textDecoration: "none",
    borderRadius: "5px",
    fontSize: "16px",
    fontWeight: "bold",
    textAlign: "center",
};

export default Dashboard;
