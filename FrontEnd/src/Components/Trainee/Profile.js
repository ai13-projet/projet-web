import React, { useState } from "react";
import BackToDashboardButton from "../header/BackToDashboardButton";
import Header from "../header/Header";
import { useParams } from 'react-router-dom';

const Profile = () => {
    const [name, setName] = useState("Jean Dupont");
    const [email, setEmail] = useState("jean.dupont@example.com");
    const [isActive, setIsActive] = useState(true);
    const params = useParams();

    const handleSave = (e) => {
        e.preventDefault();
        console.log("Nom :", name);
        console.log("Email :", email);
        console.log("Compte actif :", isActive ? "Oui" : "Non");
        alert("Profil mis à jour avec succès !");
    };

    const toggleAccountStatus = () => {
        setIsActive(!isActive);
    };

    return (
        <div style={containerStyle}>
            <Header title="Mon Profil" />
            <form onSubmit={handleSave} style={formStyle}>
                <div style={formGroupStyle}>
                    <label htmlFor="name">Nom :</label>
                    <input
                        type="text"
                        id="name"
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                        style={inputStyle}
                        required
                    />
                </div>
                <div style={formGroupStyle}>
                    <label htmlFor="email">Email :</label>
                    <input
                        type="email"
                        id="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        style={inputStyle}
                        required
                    />
                </div>
                <div style={formGroupStyle}>
                    <label>État du compte :</label>
                    <p style={{ fontWeight: "bold", color: isActive ? "green" : "red" }}>
                        {isActive ? "Actif" : "Inactif"}
                    </p>
                    <button
                        type="button"
                        onClick={toggleAccountStatus}
                        style={{
                            ...buttonStyle,
                            backgroundColor: isActive ? "#FF6347" : "#32CD32",
                        }}
                    >
                        {isActive ? "Désactiver le compte" : "Activer le compte"}
                    </button>
                </div>
                <button type="submit" style={buttonStyle}>
                    Enregistrer les modifications
                </button>
            </form>
        </div>
    );
};

const containerStyle = {
    textAlign: "center",
    padding: "20px",
    maxWidth: "600px",
    margin: "50px auto",
    backgroundColor: "#f9f9f9",
    borderRadius: "8px",
    boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
};

const formStyle = {
    display: "flex",
    flexDirection: "column",
    gap: "15px",
};

const formGroupStyle = {
    textAlign: "left",
};

const inputStyle = {
    width: "100%",
    padding: "10px",
    border: "1px solid #ccc",
    borderRadius: "4px",
    fontSize: "16px",
};

const buttonStyle = {
    padding: "10px 20px",
    backgroundColor: "#007BFF",
    color: "white",
    border: "none",
    borderRadius: "5px",
    fontSize: "16px",
    cursor: "pointer",
    marginTop: "10px",
};

export default Profile;
