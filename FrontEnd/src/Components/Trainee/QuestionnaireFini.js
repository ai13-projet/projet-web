import { Alert, AlertTitle, Button, Container, Stack, LinearProgress, Typography, Box } from "@mui/material";
import { useNavigate, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import Header from "../header/Header"; // Assurez-vous d'utiliser votre Header personnalisé

const QuizFinished = () => {
    const navigate = useNavigate();
    const params = useParams();

    const [record, setRecord] = useState({});
    const [error, setError] = useState(false);
    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(() => {
        // Simuler un appel pour récupérer les données du record
        const fetchRecord = async () => {
            try {
                // Simuler une réponse avec des données fictives
                const mockRecord = {
                    id: 1,
                    user: { id: params.traineeId, name: "Utilisateur Exemple" },
                    quiz: { label: "Exemple de Quiz" },
                    score: 8,
                    totalQuestions: 10,
                };

                setTimeout(() => {
                    setRecord(mockRecord);
                    setIsLoaded(true);
                }, 500);
            } catch {
                setError(true);
                setIsLoaded(true);
            }
        };

        fetchRecord();
    }, [params.traineeId]);

    const handleBackToQuizzes = () => {
        navigate(`/trainee/${params.traineeId}/quizzes`); // Redirige vers la liste des quiz
    };

    const handleSeeDetails = () => {
        navigate(`/trainee/${params.traineeId}/results/${record.id}/detailed`); // Redirige vers les détails du résultat
    };

    if (error) {
        return (
            <Alert severity="error">
                <AlertTitle>Erreur</AlertTitle>
                Impossible de charger les données du quiz.
            </Alert>
        );
    }

    if (!isLoaded) {
        return <LinearProgress />;
    }

    return (
        <Container maxWidth="md" sx={{ marginTop: "20px" }}>
            <Header title="Résultats du Quiz" />
            <Stack spacing={3} sx={{ marginTop: "20px" }}>
                <Box
                    sx={{
                        backgroundColor: "#f9f9f9",
                        padding: "20px",
                        borderRadius: "10px",
                        boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)",
                    }}
                >
                    <Typography variant="h5" sx={{ fontWeight: "bold", textAlign: "center" }}>
                        {record.quiz.label}
                    </Typography>
                    <Typography variant="body1" sx={{ textAlign: "center", marginTop: "10px" }}>
                        Score : {record.score} / {record.totalQuestions}
                    </Typography>
                </Box>
                <Stack alignItems="center" mt={3}>
                    <Button
                        variant="contained"
                        sx={{
                            backgroundColor: "#4A90E2",
                            color: "#fff",
                            borderRadius: "20px",
                            textTransform: "none",
                            padding: "10px 20px",
                            "&:hover": { backgroundColor: "#3b7ed0" },
                        }}
                        onClick={handleSeeDetails}
                    >
                        Voir plus de détails
                    </Button>
                </Stack>
                <Stack alignItems="center" mt={3}>
                    <Button
                        variant="outlined"
                        sx={{
                            color: "#4A90E2",
                            borderColor: "#4A90E2",
                            borderRadius: "20px",
                            textTransform: "none",
                            padding: "10px 20px",
                            "&:hover": { backgroundColor: "#f0f0f0", borderColor: "#4A90E2" },
                        }}
                        onClick={handleBackToQuizzes}
                    >
                        Retour à la liste des Quiz
                    </Button>
                </Stack>
            </Stack>
        </Container>
    );
};

export default QuizFinished;
