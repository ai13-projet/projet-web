import React, { useEffect, useState } from "react";
import BackToDashboardButton from "./BackToDashboardButton";


const Results = () => {
    const [results, setResults] = useState([]);

    // Exemple de chargement des données (remplacez par un appel API si nécessaire)
    useEffect(() => {
        const fetchResults = () => {
            const dummyResults = [
                {
                    id: 1,
                    title: "Parcours 1",
                    score: 85,
                    date: "2024-12-01",
                    details: "Vous avez réussi 17 questions sur 20.",
                },
                {
                    id: 2,
                    title: "Parcours 2",
                    score: 70,
                    date: "2024-11-20",
                    details: "Vous avez réussi 14 questions sur 20.",
                },
                {
                    id: 3,
                    title: "Parcours 3",
                    score: 90,
                    date: "2024-11-10",
                    details: "Vous avez réussi 18 questions sur 20.",
                },
            ];
            setResults(dummyResults);
        };

        fetchResults();
    }, []);

    return (
        <div style={containerStyle}>
            <h1>Résultats des Parcours</h1>
            <BackToDashboardButton />
            <ul style={listStyle}>
                {results.map((result) => (
                    <li key={result.id} style={itemStyle}>
                        <h3>{result.title}</h3>
                        <p>
                            <strong>Date :</strong> {result.date}
                        </p>
                        <p>
                            <strong>Score :</strong> {result.score}%
                        </p>
                        <p>{result.details}</p>
                    </li>
                ))}
            </ul>
        </div>
    );
};

const containerStyle = {
    textAlign: "center",
    padding: "20px",
    maxWidth: "800px",
    margin: "50px auto",
    backgroundColor: "#f9f9f9",
    borderRadius: "8px",
    boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
};

const listStyle = {
    listStyle: "none",
    padding: 0,
};

const itemStyle = {
    marginBottom: "20px",
    padding: "15px",
    border: "1px solid #ddd",
    borderRadius: "8px",
    textAlign: "left",
    backgroundColor: "#ffffff",
    boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)",
};

export default Results;
