import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Header from "../header/Header";
import ThemeService from "../../services/ThemeService";

const ThemeList = () => {
    const [themes, setThemes] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [themesPerPage] = useState(5);
    const [error, setError] = useState(null);

    const navigate = useNavigate();

    // Récupérer les thèmes depuis l'API
    useEffect(() => {
        ThemeService.getAllThemes(
            (data) =>{
                console.log(data.data)
                setThemes(data.data)
            } , // Callback en cas de succès
            (err) => {
                console.error("Erreur lors de la récupération des thèmes :", err);
                setError("Impossible de charger les thèmes.");
            }
        );
    }, []);

    // Pagination
    const indexOfLastTheme = currentPage * themesPerPage;
    const indexOfFirstTheme = indexOfLastTheme - themesPerPage;
    const currentThemes = themes.slice(indexOfFirstTheme, indexOfLastTheme);

    const totalPages = Math.ceil(themes.length / themesPerPage);

    const handlePageChange = (pageNumber) => {
        setCurrentPage(pageNumber);
    };

    // Redirection vers les questionnaires d'un thème
    const handleViewQuestionnaires = (themeId) => {
        navigate(`/themes/${themeId}/questionnaires`);
    };

    return (
        <div style={containerStyle}>
            <Header title="Liste des Thèmes" />
            {error && <p style={errorStyle}>{error}</p>}
            <ul style={listStyle}>
                {currentThemes.map((theme) => (
                    <li key={theme.id} style={itemStyle}>
                        <h3 style={{ color: "#007BFF" }}>{theme.label}</h3>
                        <p><strong>ID du Thème :</strong> {theme.id}</p>
                        <button
                            onClick={() => handleViewQuestionnaires(theme.id)}
                            style={buttonStyle}
                        >
                            Voir les questionnaires du thème
                        </button>
                    </li>
                ))}
            </ul>
            {/* Pagination */}
            <div style={paginationStyle}>
                {Array.from({ length: totalPages }, (_, index) => (
                    <button
                        key={index}
                        onClick={() => handlePageChange(index + 1)}
                        style={{
                            ...buttonStyle,
                            backgroundColor: currentPage === index + 1 ? "#007BFF" : "#f0f0f0",
                            color: currentPage === index + 1 ? "white" : "black",
                        }}
                    >
                        {index + 1}
                    </button>
                ))}
            </div>
        </div>
    );
};

const containerStyle = {
    textAlign: "center",
    padding: "20px",
    maxWidth: "800px",
    margin: "50px auto",
};

const listStyle = {
    listStyle: "none",
    padding: 0,
};

const itemStyle = {
    marginBottom: "20px",
    padding: "15px",
    border: "1px solid #ddd",
    borderRadius: "8px",
    textAlign: "left",
    backgroundColor: "#f9f9f9",
};

const buttonStyle = {
    padding: "10px 15px",
    border: "none",
    borderRadius: "5px",
    cursor: "pointer",
    fontSize: "14px",
    backgroundColor: "#28A745",
    color: "white",
};

const paginationStyle = {
    marginTop: "20px",
    display: "flex",
    justifyContent: "center",
    gap: "10px",
};

const errorStyle = {
    color: "red",
    fontWeight: "bold",
    marginBottom: "20px",
};

export default ThemeList;
