import React from "react";
import { Link } from "react-router-dom";
import Header from "../header/Header";

const Dashboard = () => {
    return (
        <div style={containerStyle}>
            <Header title="Bienvenue sur le Dashboard" />
            <p style={descriptionStyle}>
                Vous pouvez accéder aux fonctionnalités suivantes :
            </p>
            <div style={menuStyle}>
                <Link to="/visitor/questionnaires" style={linkStyle}>
                    📋 Liste des Questionnaires
                </Link>
                <Link to="/visitor/statistiques" style={linkStyle}>
                    📊 Voir les Statistiques
                </Link>
            </div>
        </div>
    );
};

const containerStyle = {
    textAlign: "center",
    padding: "20px",
    maxWidth: "600px",
    margin: "50px auto",
    backgroundColor: "#f9f9f9",
    borderRadius: "8px",
    boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
};

const descriptionStyle = {
    fontSize: "18px",
    marginBottom: "20px",
    fontWeight: "500",
    color: "#333",
};

const menuStyle = {
    display: "flex",
    flexDirection: "column",
    gap: "15px",
    marginTop: "20px",
};

const linkStyle = {
    display: "block",
    padding: "10px 20px",
    backgroundColor: "#007BFF",
    color: "white",
    textDecoration: "none",
    borderRadius: "5px",
    fontSize: "16px",
    fontWeight: "bold",
    textAlign: "center",
    transition: "all 0.3s ease",
    boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.2)",
    "&:hover": {
        backgroundColor: "#0056b3",
        transform: "scale(1.05)",
    },
};

export default Dashboard;
