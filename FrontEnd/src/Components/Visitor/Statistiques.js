import React, { useEffect, useState } from "react";
import { Card, CardContent, Typography, Box, Divider, List, ListItem, ListItemText, Pagination } from "@mui/material";
import Header from "../header/Header";
import UserService from "../../services/UserService";

const StatisticsPage = () => {
    const [userStats, setUserStats] = useState([]);
    const [error, setError] = useState(null);
    const [currentPage, setCurrentPage] = useState(1);
    const statsPerPage = 5; // Nombre de stats par page

    useEffect(() => {
        const fetchUserStats = () => {
            UserService.getAllUsers(
                (users) => {
                    const processedStats = [];
                    let remainingRequests = users.users.length;

                    // Parcourir tous les utilisateurs
                    users.users.forEach((user) => {
                        UserService.getUserStatistics(
                            user.id,
                            (stats) => {
                                // Succès : ajouter les stats de l'utilisateur
                                processedStats.push({
                                    ...stats,
                                    userName: user.name,
                                    hasStats: true,
                                });
                                checkCompletion();
                            },
                            () => {
                                // Erreur : ajouter un utilisateur sans stats
                                processedStats.push({
                                    userName: user.name,
                                    hasStats: false,
                                });
                                checkCompletion();
                            }
                        );
                    });

                    // Vérifier si toutes les requêtes sont terminées
                    const checkCompletion = () => {
                        remainingRequests -= 1;
                        if (remainingRequests === 0) {
                            setUserStats(processedStats);
                        }
                    };
                },
                (err) => {
                    // Gestion de l'erreur pour getAllUsers
                    console.error("Erreur lors de la récupération des utilisateurs :", err);
                    setError("Impossible de charger les utilisateurs.");
                }
            );
        };

        fetchUserStats();
    }, []);

    // Pagination logic
    const indexOfLastStat = currentPage * statsPerPage;
    const indexOfFirstStat = indexOfLastStat - statsPerPage;
    const currentStats = userStats.slice(indexOfFirstStat, indexOfLastStat);

    const handlePageChange = (event, value) => {
        setCurrentPage(value);
    };

    if (error) {
        return (
            <Typography variant="h6" color="error" align="center" style={{marginTop: "20px"}}>
                {error}
            </Typography>
        );
    }

    return (
        <Box sx={{padding: "20px", maxWidth: "800px", margin: "auto"}}>
            <Header title="Statistiques des Utilisateurs"/>

            {/* Affichage des statistiques filtrées */}
            {currentStats
                .filter((userStat) => userStat.hasStats) // Filtre pour les utilisateurs avec stats
                .map((userStat, index) => (
                    <Card
                        key={index}
                        sx={{
                            marginBottom: "20px",
                            borderRadius: "12px",
                            boxShadow: "0 4px 8px rgba(0,0,0,0.1)",
                            backgroundColor: "#f4f6ff",
                        }}
                    >
                        <CardContent>
                            {/* Titre */}
                            <Typography
                                variant="h6"
                                sx={{fontWeight: "bold", color: "#3b5998", marginBottom: "10px"}}
                            >
                                {userStat.userId}'s Statistiques
                            </Typography>
                            <Divider sx={{marginBottom: "10px"}}/>

                            {/* Détails des statistiques */}
                            <Typography variant="body1" sx={{marginBottom: "10px"}}>
                                <strong>Score moyen :</strong> {userStat.averageScore}%
                            </Typography>
                            <Typography variant="body1" sx={{marginBottom: "10px"}}>
                                <strong>Temps moyen :</strong> {userStat.averageTime} minutes
                            </Typography>
                            <Typography variant="body1" sx={{marginBottom: "10px"}}>
                                <strong>Nombre de quiz :</strong> {userStat.numberOfQuizzes}
                            </Typography>

                            {/* Classements par Quiz */}
                            <Box
                                sx={{marginTop: "10px", backgroundColor: "#eef", padding: "10px", borderRadius: "8px"}}>
                                <Typography variant="subtitle1" sx={{fontWeight: "bold", marginBottom: "8px"}}>
                                    📊 Classements par Quiz
                                </Typography>
                                <List dense>
                                    {userStat.rankings.map((ranking, idx) => (
                                        <ListItem
                                            key={idx}
                                            sx={{
                                                padding: "6px 12px",
                                                borderRadius: "8px",
                                                "&:nth-of-type(odd)": {backgroundColor: "#e8f0fe"},
                                                "&:hover": {backgroundColor: "#d0e3ff"},
                                            }}
                                        >
                                            <ListItemText
                                                primary={
                                                    <Typography variant="body1" sx={{fontWeight: "bold"}}>
                                                        {ranking.quizTitle}
                                                    </Typography>
                                                }
                                                secondary={
                                                    <Typography variant="body2" sx={{color: "#555"}}>
                                                        Rang #{ranking.rank} avec un score de{" "}
                                                        <span style={{fontWeight: "bold", color: "#007BFF"}}>
                                                        {isFinite(ranking.scoreRatio)
                                                            ? `${(ranking.scoreRatio * 100).toFixed(2)}%`
                                                            : "N/A"}
                                                    </span>
                                                    </Typography>
                                                }
                                            />
                                        </ListItem>
                                    ))}
                                </List>
                            </Box>
                        </CardContent>
                    </Card>
                ))}

            {/* Message si aucun utilisateur avec statistiques */}
            {currentStats.filter((userStat) => userStat.hasStats).length === 0 && (
                <Typography variant="h6" sx={{textAlign: "center", color: "#888"}}>
                    Aucun utilisateur avec des statistiques disponibles.
                </Typography>
            )}

            {/* Pagination */}
            <Box sx={{display: "flex", justifyContent: "center", marginTop: "20px"}}>
                <Pagination
                    count={Math.ceil(userStats.filter((userStat) => userStat.hasStats).length / statsPerPage)}
                    page={currentPage}
                    onChange={handlePageChange}
                    color="primary"
                />
            </Box>
        </Box>
    );
}

    export default StatisticsPage;
