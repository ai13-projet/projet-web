import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import BackToDashboardButton from "../header/BackToDashboardButton";
import Header from "../header/Header";

const Results = () => {
    const { id } = useParams(); // Récupère l'ID du questionnaire depuis l'URL
    const [results, setResults] = useState(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        // Exemple de chargement des résultats basés sur l'ID du questionnaire
        const fetchResults = async () => {
            // Simule une API avec des données fictives
            const dummyData = {
                1: {
                    title: "Questionnaire 1",
                    score: 85,
                    duration: "15 minutes",
                    rank: 3,
                    details: [
                        { question: "Question 1", answer: "Correct", time: "2 minutes" },
                        { question: "Question 2", answer: "Incorrect", time: "3 minutes" },
                    ],
                },
                2: {
                    title: "Questionnaire 2",
                    score: 70,
                    duration: "20 minutes",
                    rank: 5,
                    details: [
                        { question: "Question 1", answer: "Correct", time: "4 minutes" },
                        { question: "Question 2", answer: "Correct", time: "2 minutes" },
                    ],
                },
            };

            // Simule un délai pour le chargement
            setTimeout(() => {
                setResults(dummyData[id] || null); // Charge les résultats correspondants
                setLoading(false);
            }, 1000);
        };

        fetchResults();
    }, [id]);

    if (loading) {
        return <p style={{ textAlign: "center" }}>Chargement des résultats...</p>;
    }

    if (!results) {
        return <p style={{ textAlign: "center" }}>Aucun résultat trouvé pour ce questionnaire.</p>;
    }

    return (
        <div style={containerStyle}>
            <h1>Résultats - {results.title}</h1>
            <Header title="Résultats - {results.title}" />
            <div style={resultItemStyle}>
                <p>
                    <strong>Score :</strong> {results.score}%
                </p>
                <p>
                    <strong>Durée :</strong> {results.duration}
                </p>
                <p>
                    <strong>Classement :</strong> {results.rank}{results.rank === 1 ? "er" : "e"}
                </p>
            </div>
            <h3>Détails des Questions :</h3>
            <ul style={detailsStyle}>
                {results.details.map((detail, index) => (
                    <li key={index} style={detailItemStyle}>
                        <p>
                            <strong>{detail.question}</strong>
                        </p>
                        <p>
                            <strong>Réponse :</strong> {detail.answer}
                        </p>
                        <p>
                            <strong>Temps :</strong> {detail.time}
                        </p>
                    </li>
                ))}
            </ul>
        </div>
    );
};

const containerStyle = {
    textAlign: "center",
    padding: "20px",
    maxWidth: "800px",
    margin: "50px auto",
    backgroundColor: "#f9f9f9",
    borderRadius: "8px",
    boxShadow: "0px 4px 6px rgba(0, 0, 0, 0.1)",
};

const resultItemStyle = {
    marginBottom: "20px",
    padding: "15px",
    border: "1px solid #ddd",
    borderRadius: "8px",
    textAlign: "left",
    backgroundColor: "#ffffff",
    boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)",
};

const detailsStyle = {
    listStyle: "none",
    padding: 0,
};

const detailItemStyle = {
    marginBottom: "15px",
    padding: "10px",
    border: "1px solid #ddd",
    borderRadius: "8px",
    backgroundColor: "#f9f9f9",
};

export default Results;
