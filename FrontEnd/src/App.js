import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import Home from "./pages/Home";
import Login from "./pages/Login";

//TRAINEE
import Dashboard from "./Components/Trainee/Dashboard";
import QuestionnaireList from "./Components/Trainee/QuestionnaireList";
import Results from "./Components/Trainee/Results";
import Profile from "./Components/Trainee/Profile";
import QuestionnaireActif from "./Components/Trainee/QuestionnaireActif";
import QuestionnaireFini from "./Components/Trainee/QuestionnaireFini";
import QuestionnaireFiniDetail from "./Components/Trainee/QuestionnaireFiniDetail";

//VISITOR
import DashboardVisit from "./Components/Visitor/Dashboard";
import ThemeListVisit from "./Components/Visitor/ThemeList";
import ThemeQuizzList from "./Components/Visitor/ThemeQuizzList";
import Statistiques from "./Components/Visitor/Statistiques";

const App = () => {
    return (
        <Router>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/login" element={<Login />} />

                {/* TRAINEE ROUTES */}
                <Route path="/trainee/:traineeId/dashboard" element={<Dashboard />} />
                <Route path="/trainee/:traineeId/questionnaires" element={<QuestionnaireList />} />
                <Route path="/trainee/:traineeId/results" element={<Results />} />
                <Route path="/trainee/:traineeId/profile" element={<Profile />} />
                <Route path="/trainee/:traineeId/quizz/:quizzId" element={<QuestionnaireActif />} />
                <Route path="/trainee/:traineeId/results/:quizzId" element={<QuestionnaireFini />} />
                <Route path="/trainee/:traineeId/results/:quizzId/detailed" element={< QuestionnaireFiniDetail />} />


                {/* VISITOR ROUTES */}
                <Route path="/visitor/dashboard" element={<DashboardVisit />} />
                <Route path="/visitor/questionnaires" element={<ThemeListVisit />} />
                <Route path="/visitor/questionnaires/:themeId" element={<ThemeQuizzList />} />
                <Route path="/visitor/statistiques" element={<Statistiques />} />

            </Routes>
        </Router>
    );
};

export default App;
