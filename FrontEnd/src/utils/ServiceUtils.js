import axios from "axios";

export const API = axios.create({
  baseURL: "http://localhost:8080",
});

export const setAuthorizationToken = (token) => {
  if (token) {
    API.defaults.headers["Authorization"] = `Bearer ${token}`;
  } else {
    delete API.defaults.headers["Authorization"];
  }
};