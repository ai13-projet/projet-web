import { API } from "../utils/ServiceUtils";

export default class AuthService {
  /**
   * Effectuer une authentification (login)
   * @param {Object} credentials - Les identifiants de l'utilisateur (email et password)
   * @param {Function} callback - Fonction appelée en cas de succès
   * @param {Function} errorCallback - Fonction appelée en cas d'erreur
   */
  static login(credentials, callback, errorCallback) {
      API.post(`/auth/login`, credentials)
        .then((response) => {
          callback(response.data);
        })
        .catch((error) => {
          errorCallback(error.response?.data || error.message);
        });
  }
}
