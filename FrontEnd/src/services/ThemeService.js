import { API } from "../utils/ServiceUtils";

export default class ThemeService {
  /**
   * Récupérer tous les thèmes
   * @param {Function} callback - Fonction appelée en cas de succès
   * @param {Function} errorCallback - Fonction appelée en cas d'erreur
   */
  static getAllThemes(callback, errorCallback) {
    API.get(`/themes/all`)
        .then((response) => callback(response.data))
        .catch((error) => errorCallback(error.response?.data || error.message));
  }
}
