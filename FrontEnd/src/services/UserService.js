import { API } from "../utils/ServiceUtils";

export default class UserService {
  /**
   * Récupérer tous les utilisateurs
   * @param {Function} callback - Fonction appelée en cas de succès
   * @param {Function} errorCallback - Fonction appelée en cas d'erreur
   */
  static getAllUsers(callback, errorCallback) {
    API.get(`/users/all`)
        .then((response) => callback(response.data))
        .catch((error) => errorCallback(error.response?.data || error.message));
  }

  /**
   * Récupérer un utilisateur par ID
   * @param {Number} userId - ID de l'utilisateur
   * @param {Function} callback - Fonction appelée en cas de succès
   * @param {Function} errorCallback - Fonction appelée en cas d'erreur
   */
  static getUserById(userId, callback, errorCallback) {
    API.get(`/users/${userId}`)
        .then((response) => callback(response.data))
        .catch((error) => errorCallback(error.response?.data || error.message));
  }

  /**
   * Récupère les statistiques d'un utilisateur par son ID.
   * @param {number} userId - ID de l'utilisateur.
   * @param {function} callback - Fonction appelée en cas de succès.
   * @param {function} errorCallback - Fonction appelée en cas d'erreur.
   */
  static getUserStatistics(userId, callback, errorCallback) {
    API.get(`/users/${userId}/stats`)
        .then((response) => {
          if (response.data.status === "success") {
            callback(response.data.data);
          } else if (response.data.status === "info") {
            console.warn(response.data.message);
            callback(null);
          }
        })
        .catch((error) => {
          console.error("Erreur lors de la récupération des statistiques :", error);
          errorCallback(error);
        });
  }
}
