import { API } from "../utils/ServiceUtils";

export default class QuizService {
  /**
   * Obtenir les quizzes créés par un utilisateur
   * @param {Number} userId - ID de l'utilisateur
   * @param {Function} callback - Fonction appelée en cas de succès
   * @param {Function} errorCallback - Fonction appelée en cas d'erreur
   */
  static getQuizzesCreatedByUser(userId, callback, errorCallback) {
    API.get(`/quizzes/user/${userId}/created`)
        .then((response) => callback(response.data))
        .catch((error) => errorCallback(error.response?.data || error.message));
  }

  /**
   * Obtenir les quizzes créés par un utilisateur
   * @param {Number} userId - ID de l'utilisateur
   * @param {Function} callback - Fonction appelée en cas de succès
   * @param {Function} errorCallback - Fonction appelée en cas d'erreur
   */
  static getActiveQuizzes(callback, errorCallback) {
    API.get(`/quizzes/active`)
        .then((response) => callback(response.data))
        .catch((error) => errorCallback(error.response?.data || error.message));
  }
}
