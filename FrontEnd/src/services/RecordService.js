import { API } from "../utils/ServiceUtils";

export default class RecordService {
  /**
   * Créer un enregistrement pour un utilisateur et un quiz spécifiques
   * @param {Number} userId - ID de l'utilisateur
   * @param {Number} quizId - ID du quiz
   * @param {Object} recordDTO - Données de l'enregistrement
   * @param {Function} callback - Fonction appelée en cas de succès
   * @param {Function} errorCallback - Fonction appelée en cas d'erreur
   */
  static createRecord(userId, quizId, recordDTO, callback, errorCallback) {
    API.post(`/records/users/${userId}/quizzes/${quizId}`, recordDTO)
        .then((response) => callback(response.data))
        .catch((error) => errorCallback(error.response?.data || error.message));
  }

  /**
   * Récupérer les enregistrements d'un utilisateur
   * @param {Number} userId - ID de l'utilisateur
   * @param {Function} callback - Fonction appelée en cas de succès
   * @param {Function} errorCallback - Fonction appelée en cas d'erreur
   */
  static getRecordsByUser(userId, callback, errorCallback) {
    API.get(`/records/users/${userId}`)
        .then((response) => callback(response.data))
        .catch((error) => errorCallback(error.response?.data || error.message));
  }

  /**
   * Récupérer les enregistrements d'un quiz
   * @param {Number} quizId - ID du quiz
   * @param {Function} callback - Fonction appelée en cas de succès
   * @param {Function} errorCallback - Fonction appelée en cas d'erreur
   */
  static getRecordsByQuiz(quizId, callback, errorCallback) {
    API.get(`/records/quizzes/${quizId}`)
        .then((response) => callback(response.data))
        .catch((error) => errorCallback(error.response?.data || error.message));
  }

  /**
   * Ajouter des réponses à un enregistrement
   * @param {Number} recordId - ID de l'enregistrement
   * @param {Array} detailsDTO - Liste des réponses
   * @param {Function} callback - Fonction appelée en cas de succès
   * @param {Function} errorCallback - Fonction appelée en cas d'erreur
   */
  static addAnswersToRecord(recordId, detailsDTO, callback, errorCallback) {
    API.post(`/records/${recordId}/answers`, detailsDTO)
        .then((response) => callback(response.data))
        .catch((error) => errorCallback(error.response?.data || error.message));
  }

  /**
   * Récupérer les résultats d'un utilisateur
   * @param {Number} userId - ID de l'utilisateur
   * @param {Function} callback - Fonction appelée en cas de succès
   * @param {Function} errorCallback - Fonction appelée en cas d'erreur
   */
  static getResultsByUser(userId, callback, errorCallback) {
    API.get(`/records/users/${userId}/results`)
        .then((response) => callback(response.data))
        .catch((error) => errorCallback(error.response?.data || error.message));
  }

  /**
   * Récupérer les résultats d'un quiz
   * @param {Number} quizId - ID du quiz
   * @param {Function} callback - Fonction appelée en cas de succès
   * @param {Function} errorCallback - Fonction appelée en cas d'erreur
   */
  static getResultsByQuiz(quizId, callback, errorCallback) {
    API.get(`/records/quizzes/${quizId}/results`)
        .then((response) => callback(response.data))
        .catch((error) => errorCallback(error.response?.data || error.message));
  }
}
