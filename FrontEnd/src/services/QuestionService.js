import { API } from "../utils/ServiceUtils";


export default class QuestionService {
    /**
     * Récupère les questions associées à un quiz via son ID.
     * @param {number} quizId - ID du quiz pour lequel récupérer les questions.
     * @param {function} callback - Fonction appelée en cas de succès.
     * @param {function} errorCallback - Fonction appelée en cas d'erreur.
     */
    static getQuestionsByQuiz(quizId, callback, errorCallback) {
        API.get(`/questions/quiz/${quizId}`)
            .then((response) => {
                callback(response.data); // Retourne les questions sous "data"
            })
            .catch((error) => {
                errorCallback(error);
            });
    }

    /**
     * Ajoute une question à un quiz.
     * @param {Object} questionData - Données de la nouvelle question.
     * @param {function} callback - Fonction appelée en cas de succès.
     * @param {function} errorCallback - Fonction appelée en cas d'erreur.
     */
    static addQuestion(questionData, callback, errorCallback) {
        API.post(`/questions`, questionData)
            .then((response) => {
                callback(response.data);
            })
            .catch((error) => {
                errorCallback(error);
            });
    }

    /**
     * Met à jour une question existante.
     * @param {number} questionId - ID de la question à mettre à jour.
     * @param {Object} questionData - Nouvelles données pour la question.
     * @param {function} callback - Fonction appelée en cas de succès.
     * @param {function} errorCallback - Fonction appelée en cas d'erreur.
     */
    static updateQuestion(questionId, questionData, callback, errorCallback) {
        API.put(`/questions/${questionId}`, questionData)
            .then((response) => {
                callback(response.data);
            })
            .catch((error) => {
                errorCallback(error);
            });
    }

    /**
     * Supprime une question.
     * @param {number} questionId - ID de la question à supprimer.
     * @param {function} callback - Fonction appelée en cas de succès.
     * @param {function} errorCallback - Fonction appelée en cas d'erreur.
     */
    static deleteQuestion(questionId, callback, errorCallback) {
        API.delete(`/questions/${questionId}`)
            .then(() => {
                callback({ message: "Question supprimée avec succès." });
            })
            .catch((error) => {
                errorCallback(error);
            });
    }

    /**
     * Modifie la position d'une question dans un quiz.
     * @param {number} questionId - ID de la question à déplacer.
     * @param {number} newPosition - Nouvelle position de la question.
     * @param {function} callback - Fonction appelée en cas de succès.
     * @param {function} errorCallback - Fonction appelée en cas d'erreur.
     */
    static updateQuestionPosition(questionId, newPosition, callback, errorCallback) {
        API.put(`/questions/${questionId}/position`, null, {
            params: { newPosition },
        })
            .then((response) => {
                callback(response.data);
            })
            .catch((error) => {
                errorCallback(error);
            });
    }

    /**
     * Active ou désactive une question.
     * @param {number} questionId - ID de la question.
     * @param {boolean} isActive - Statut actif (true) ou inactif (false).
     * @param {function} callback - Fonction appelée en cas de succès.
     * @param {function} errorCallback - Fonction appelée en cas d'erreur.
     */
    static updateQuestionStatus(questionId, isActive, callback, errorCallback) {
        API.put(`/questions/${questionId}/status`, null, {
            params: { isActive },
        })
            .then((response) => {
                callback(response.data);
            })
            .catch((error) => {
                errorCallback(error);
            });
    }
}
