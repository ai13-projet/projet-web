package com.example.AI13_Projet.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;

@Component
public class JwtUtil {

    private final Key secretKey = Keys.secretKeyFor(SignatureAlgorithm.HS512);

    public String generateToken(String email, String role) {
        return Jwts.builder()
                .setSubject(email) // Set the email as the subject
                .claim("role", role) // Add the single role as a custom claim
                .setIssuedAt(new Date()) // Set token issue time
                .setExpiration(new Date(System.currentTimeMillis() + 3600 * 1000)) // Token valid for 1 hour
                .signWith(secretKey, SignatureAlgorithm.HS512) // Sign with secret key and algorithm
                .compact();
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parserBuilder()
                    .setSigningKey(secretKey)
                    .build()
                    .parseClaimsJws(token);
            return true;
        } catch (io.jsonwebtoken.ExpiredJwtException e) {
            System.out.println("Token expired: " + e.getMessage());
        } catch (io.jsonwebtoken.MalformedJwtException e) {
            System.out.println("Malformed token: " + e.getMessage());
        } catch (io.jsonwebtoken.SignatureException e) {
            System.out.println("Invalid signature: " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Token validation failed: " + e.getMessage());
        }
        return false;
    }


    public String extractEmail(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(secretKey)
                .build()
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
    }

    public String extractRole(String token) {
        Claims claims = Jwts.parserBuilder()
                .setSigningKey(secretKey)
                .build()
                .parseClaimsJws(token)
                .getBody();

        return claims.get("role", String.class); // Extract the "role" claim
    }

    public void testJwt() {
        JwtUtil jwtUtil = new JwtUtil();
        String token = jwtUtil.generateToken("saad.lakramti@etu.utc.fr", "trainee");
        System.out.println("Generated Token: " + token);

        boolean isValid = jwtUtil.validateToken(token);
        System.out.println("Is Token Valid: " + isValid);

        if (isValid) {
            String email = jwtUtil.extractEmail(token);
            String role = jwtUtil.extractRole(token);
            System.out.println("Email: " + email);
            System.out.println("Role: " + role);
        }
    }
}
