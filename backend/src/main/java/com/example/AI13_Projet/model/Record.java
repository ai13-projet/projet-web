package com.example.AI13_Projet.model;

import java.time.LocalDateTime;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;

@Entity
public class Record {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user; // Référence à l'utilisateur

    @ManyToOne
    @JoinColumn(name = "quiz_id", nullable = false)
    private Quiz quiz; // Référence au questionnaire

    private int score; // Score obtenu

    private int duration; // Durée du parcours en secondes


    private LocalDateTime startedAt; // Date et heure de début du parcours

    private LocalDateTime completedAt; // Date et heure de fin du parcours

    @OneToMany(mappedBy = "record", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RecordDetail> details; // Liste des réponses associées au parcours

    // Constructeurs
    public Record() {}

    public Record(Long id, User user, Quiz quiz, int score, int duration, LocalDateTime startedAt, LocalDateTime completedAt, List<RecordDetail> details) {
        this.id = id;
        this.user = user;
        this.quiz = quiz;
        this.score = score;
        this.duration = duration;
        this.startedAt = startedAt;
        this.completedAt = completedAt;
        this.details = details;
    }

    // Getters et Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public LocalDateTime getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(LocalDateTime startedAt) {
        this.startedAt = startedAt;
    }

    public LocalDateTime getCompletedAt() {
        return completedAt;
    }

    public void setCompletedAt(LocalDateTime completedAt) {
        this.completedAt = completedAt;
    }

    public List<RecordDetail> getDetails() {
        return details;
    }

    public void setDetails(List<RecordDetail> details) {
        this.details = details;
    }
}
