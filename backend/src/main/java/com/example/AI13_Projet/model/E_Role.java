package com.example.AI13_Projet.model;


public enum E_Role {
    ADMIN("Admin"),
    TRAINEE("Trainee");

    private final String label;

    E_Role(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
