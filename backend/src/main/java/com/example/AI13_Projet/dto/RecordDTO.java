package com.example.AI13_Projet.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public class RecordDTO implements Serializable {

    private Long id;
    private Long userId; // Référence à l'utilisateur
    private Long quizId; // Référence au questionnaire
    private Integer score; // Score obtenu par l'utilisateur
    private Integer duration; // Durée du passage en secondes
    private LocalDateTime completedAt; // Date et heure de fin du parcours
    private List<RecordDetailDTO> details; // Détails des réponses données par l'utilisateur

    // Constructeurs
    public RecordDTO() {}

    public RecordDTO(Long id, Long userId, Long quizId, int score, int duration, LocalDateTime completedAt, List<RecordDetailDTO> details) {
        this.id = id;
        this.userId = userId;
        this.quizId = quizId;
        this.score = score;
        this.duration = duration;
        this.completedAt = completedAt;
        this.details = details;
    }

    // Getters et Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public LocalDateTime getCompletedAt() {
        return completedAt;
    }

    public void setCompletedAt(LocalDateTime completedAt) {
        this.completedAt = completedAt;
    }

    public List<RecordDetailDTO> getDetails() {
        return details;
    }

    public void setDetails(List<RecordDetailDTO> details) {
        this.details = details;
    }
}

