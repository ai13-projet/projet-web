package com.example.AI13_Projet.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class QuestionDTO implements Serializable {

    private Long id;
    private Long quizId;
    private String label;
    private Integer position;

    @JsonProperty("isActive")
    private Boolean isActive; // Actif ou inactif
    private List<AnswerDTO> answers; // Liste des réponses associées

    // Constructeurs
    public QuestionDTO() {}

    public QuestionDTO(Long id, Long quizId, String label, Integer position, Boolean isActive, List<AnswerDTO> answers) {
        this.id = id;
        this.quizId = quizId;
        this.label = label;
        this.position = position;
        this.isActive = isActive;
        this.answers = answers;
    }

    // Getters et Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Boolean getStatus() {
        return isActive;
    }

    public void setStatus(Boolean status) {
        this.isActive = status;
    }

    public List<AnswerDTO> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerDTO> answers) {
        this.answers = answers;
    }
}
