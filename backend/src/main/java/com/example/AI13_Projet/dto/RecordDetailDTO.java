package com.example.AI13_Projet.dto;

import java.io.Serializable;

public class RecordDetailDTO implements Serializable {

    private Long questionId; // Référence à la question
    private Long answerId;   // Réponse donnée par l'utilisateur
    private boolean isCorrect; // Indique si la réponse est correcte

    // Constructeurs
    public RecordDetailDTO() {}

    public RecordDetailDTO(Long questionId, Long answerId, boolean isCorrect) {
        this.questionId = questionId;
        this.answerId = answerId;
        this.isCorrect = isCorrect;
    }

    // Getters et Setters
    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public Long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Long answerId) {
        this.answerId = answerId;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }
}
