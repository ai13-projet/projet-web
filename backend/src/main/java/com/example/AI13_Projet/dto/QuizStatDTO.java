package com.example.AI13_Projet.dto;

import com.example.AI13_Projet.model.User;

import java.util.List;

public class QuizStatDTO {
    private Long quizId;
    private String quizTitle;  // Titre du quiz
    private double averageScore;            // Moyenne des scores
    private double averageTime;             // Moyenne du temps passé
    private int numberOfRespondents;        // Nombre de stagiaires ayant répondu
    private List<User> ranking;

    // Constructeurs
    public QuizStatDTO() {}

    // Getters et setters
    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }

    public String getQuizTitle() {
        return quizTitle;
    }

    public void setQuizTitle(String quizTitle) {
        this.quizTitle = quizTitle;
    }

    public double getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(double averageScore) {
        this.averageScore = averageScore;
    }

    public double getAverageTime() {
        return averageTime;
    }

    public void setAverageTime(double averageTime) {
        this.averageTime = averageTime;
    }

    public int getNumberOfRespondents() {
        return numberOfRespondents;
    }

    public void setNumberOfRespondents(int numberOfRespondents) {
        this.numberOfRespondents = numberOfRespondents;
    }

    public List<User> getRanking() {
        return ranking;
    }

    public void setRanking(List<User> ranking) {
        this.ranking = ranking;
    }
}
