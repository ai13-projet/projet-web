package com.example.AI13_Projet.dto;

public class QuizDTO {
        private Long id;
        private Long creatorId;    // Optional: if needed for verification
        private String title;
        private String themeLabel; // Use label instead of ID
        private boolean isActive;  // Optional: if needed for verification
        private int version;

        // Getters et setters

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getThemeLabel() {
            return themeLabel;
        }

        public void setThemeLabel(String themeLabel) {
            this.themeLabel = themeLabel;
        }

        public Long getCreatorId() {
            return creatorId;
        }

        public void setCreatorId(Long creatorId) {
            this.creatorId = creatorId;
        }

        public boolean isActive() { return isActive; }

        public void setActive(boolean isActive) { this.isActive = isActive; }

        public int getVersion() { return version; }

        public void setVersion(int version) { this.version = version; }
}




