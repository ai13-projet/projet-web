package com.example.AI13_Projet.dto;

public class QuizRankingDTO {
    private Long quizId;       // ID du quiz
    private String quizTitle;  // Titre du quiz
    private int rank;          // Classement de l'utilisateur
    private double scoreRatio; // Rapport score/durée

    // Constructeurs
    public QuizRankingDTO() {}

    // Getters et setters
    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }

    public String getQuizTitle() {
        return quizTitle;
    }

    public void setQuizTitle(String quizTitle) {
        this.quizTitle = quizTitle;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public double getScoreRatio() {
        return scoreRatio;
    }

    public void setScoreRatio(double scoreRatio) {
        this.scoreRatio = scoreRatio;
    }
}