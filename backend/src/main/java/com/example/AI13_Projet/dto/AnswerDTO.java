package com.example.AI13_Projet.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class AnswerDTO implements Serializable {

    private Long id;
    private Long questionId;
    private String label;
    private Boolean isCorrect; // Boolean for consistency
    private Integer position;
    private Boolean isActive;  // Boolean for consistency

    // Constructors
    public AnswerDTO() {}

    public AnswerDTO(Long id, Long questionId, String label, Boolean isCorrect, int position, Boolean isActive) {
        this.id = id;
        this.questionId = questionId;
        this.label = label;
        this.isCorrect = isCorrect;
        this.position = position;
        this.isActive = isActive;
    }

    // Getters et Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Boolean isCorrect() { // Correct getter name
        return isCorrect;
    }

    public void setCorrect(Boolean isCorrect) {
        this.isCorrect = isCorrect;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Boolean isActive() { // Correct getter name
        return isActive;
    }

    public void setActive(Boolean isActive) {
        this.isActive = isActive;
    }
}
