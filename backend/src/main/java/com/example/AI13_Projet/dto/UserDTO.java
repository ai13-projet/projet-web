package com.example.AI13_Projet.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDTO {

    private Long id;

    private String firstname;
    private String lastname;
    private String email;
    private boolean isActive;
    private String company;
    private String phone;

    @JsonProperty("role")
    private String role;


    // Constructeurs, getters et setters
    public UserDTO() {}

    public UserDTO(Long id, String firstname, String lastname, String email,boolean isActive, String company, String phone, String role) {

        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.isActive = isActive;
        this.company = company;
        this.phone = phone;
        this.role = role;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                ", company='" + company + '\'' +
                ", phone='" + phone + '\'' +
                ", role='" + role + '\'' +
                '}';
    }

    // Getters et setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPhone() {
        return phone;
    }

    public String getRole(){return role;}

    public void setRole(String r) {
        this.role = r;
    }

    public boolean isValidRole(){
        if (getRole()==null){
            throw new IllegalArgumentException("IsValidRole: getRole => role is null");
        }

        return getRole().equals("admin") || getRole().equals("trainee");
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
