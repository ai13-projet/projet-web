package com.example.AI13_Projet.dto;

import java.util.List;

public class UserStatDTO {
    private Long userId;
    private double averageScore;    // Moyenne des scores
    private double averageTime;     // Moyenne du temps passé
    private int numberOfQuizzes;    // Nombre de quiz répondus
    private List<QuizRankingDTO> rankings;  // Classement de l'utilisateur pour chaque quiz

    // Constructeurs
    public UserStatDTO() {}

    // Getters et setters
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public double getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(double averageScore) {
        this.averageScore = averageScore;
    }

    public double getAverageTime() {
        return averageTime;
    }

    public void setAverageTime(double averageTime) {
        this.averageTime = averageTime;
    }

    public int getNumberOfQuizzes() {
        return numberOfQuizzes;
    }

    public void setNumberOfQuizzes(int numberOfQuizzes) {
        this.numberOfQuizzes = numberOfQuizzes;
    }

    public List<QuizRankingDTO> getRankings() {
        return rankings;
    }

    public void setRankings(List<QuizRankingDTO> rankings) {
        this.rankings = rankings;
    }
}
