package com.example.AI13_Projet.filter;

import com.example.AI13_Projet.utils.JwtUtil;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.List;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final JwtUtil jwtUtil;

    public JwtAuthenticationFilter(JwtUtil jwtUtil) {
        this.jwtUtil = jwtUtil;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        if ("/auth/login".equals(request.getRequestURI()) || "/users/all".equals(request.getRequestURI()) || request.getRequestURI().matches("/users/[1-9]\\d*/stats") || request.getRequestURI().matches("/themes/all") || request.getRequestURI().matches("/themes/[1-9]\\d*/quizzes") || request.getRequestURI().matches("/quizzes/[1-9]\\d*/stats")) {
            System.out.println("Before Filter Chain");
            // Skip processing for /auth/login
            filterChain.doFilter(request, response);
            return;
        }

        System.out.println("Inside JwtAuthenticationFilter");

        String token = extractToken(request);

        if (token == null) {
            // Token is missing, respond with 401 Unauthorized
            handleUnauthorized(response, "Missing token.");
            return;
        }


        System.out.println("Token:"+token);

        try {
            System.out.println("Token is valid:"+jwtUtil.validateToken(token));
            if (jwtUtil.validateToken(token)) {
                // Extract email and role from the token
                String email = jwtUtil.extractEmail(token);
                String role = jwtUtil.extractRole(token);

                System.out.println("Extracted email: " + email);
                System.out.println("Extracted role: " + role);

                List<SimpleGrantedAuthority> authorities = List.of(new SimpleGrantedAuthority("ROLE_" + role));

                System.out.println("Authenticated user: " + SecurityContextHolder.getContext().getAuthentication());


                // Set the authentication in the SecurityContext
                UsernamePasswordAuthenticationToken authentication =
                        new UsernamePasswordAuthenticationToken(new User(email, "", authorities), null, authorities);
                SecurityContextHolder.getContext().setAuthentication(authentication);
            } else {
                // Token is invalid
                handleUnauthorized(response, "Invalid token.");
                return;
            }
        } catch (Exception e) {
            // Token validation failed due to an exception (e.g., expired token)
            handleUnauthorized(response, "Token validation failed: " + e.getMessage());
            return;
        }

        // Continue the filter chain if the token is valid
        filterChain.doFilter(request, response);
    }

    private String extractToken(HttpServletRequest request) {
        String header = request.getHeader("Authorization");
        if (header != null && header.startsWith("Bearer ")) {
            return header.substring(7); // Remove "Bearer " prefix
        }
        return null;
    }

    private void handleUnauthorized(HttpServletResponse response, String message) throws IOException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED); // 401 status code
        response.setContentType("application/json");
        response.getWriter().write("{\"error\": \"" + message + "\"}");
        response.getWriter().flush();
    }
}
