package com.example.AI13_Projet.config;

import com.example.AI13_Projet.filter.JwtAuthenticationFilter;
import com.example.AI13_Projet.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import com.example.AI13_Projet.filter.CustomCorsFilter;


@Configuration
public class SecurityConfig {

    private final JwtAuthenticationFilter jwtAuthenticationFilter;
    private final CustomCorsFilter customCorsFilter;



    public SecurityConfig(JwtAuthenticationFilter jwtAuthenticationFilter, CustomCorsFilter customCorsFilter) {
        this.jwtAuthenticationFilter = jwtAuthenticationFilter;
        this.customCorsFilter = customCorsFilter;
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        System.out.println("In Security.config");
        http
                .csrf(AbstractHttpConfigurer::disable)  // Disable CSRF for simplicity
                .addFilterBefore(customCorsFilter, UsernamePasswordAuthenticationFilter.class) // Ajout du filtre CORS
                .authorizeHttpRequests(auth -> auth
                        .requestMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                        .requestMatchers("/auth/login").permitAll() // Allow access to /auth/login for everyone
                        .requestMatchers("/users/all").permitAll() // Allow access to /auth/login for everyone
                        .requestMatchers("/users/{userId}/stats").permitAll()
                        .requestMatchers("/themes/{themeId}/quizzes").permitAll()
                        .requestMatchers("/themes/all").permitAll()
                        .requestMatchers("/quizzes/{quizId}/stats").permitAll()

                        .requestMatchers("/quizzes").hasAnyRole("admin", "trainee")

                        .requestMatchers("/quizzes/user/{userId}").hasRole("admin")
                        .requestMatchers("/quizzes/active").hasRole("admin")
                        .requestMatchers("/quizzes/{quizId}/status").hasRole("admin")
                        .requestMatchers("/quizzes/{quizId}").hasRole("admin")
                        .requestMatchers("/quizzes/user/{userId}/created").hasRole("admin")
                        .requestMatchers("/quizzes/{quizId}/versions").hasRole("admin")
                        .requestMatchers("/quizzes/{quizId}/latest").hasRole("admin")

                        .requestMatchers("/users").hasRole("admin")
                        .requestMatchers("/users/{id}").hasRole("admin")
                        .requestMatchers("/users/{id}/status").hasRole("admin")

                        .requestMatchers("/answers").hasRole("trainee")
                        .requestMatchers("/answers/{id}").hasRole("admin")
                        .requestMatchers("/answers/question/{questionId}").hasRole("admin")
                        .requestMatchers("/answers/{answerId}/position").hasRole("admin")
                        .requestMatchers("/answers/{id}/status").hasRole("admin")

                        .requestMatchers("/questions/**").hasRole("admin")

                        .requestMatchers("/records/users/{userId}/quizzes/{quizId}").hasRole("trainee")
                        .requestMatchers("/records/users/{userId}").hasRole("admin")
                        .requestMatchers("/records/quizzes/{quizId}").hasRole("admin")

                        .requestMatchers("/records/{recordId}").hasAnyRole("admin", "trainee")
                        .requestMatchers("/records/{recordId}/answers").hasAnyRole("admin", "trainee")

                        .requestMatchers("/records/{recordId}/delete").hasRole("admin")

                        .requestMatchers("/themes").hasRole("admin")
                        .requestMatchers("/themes/{themeId}").hasRole("admin")

                        //.requestMatchers("/users").hasRole("admin")
                        .anyRequest().authenticated() // Require authentication for all other endpoints
                )
                .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class); // Add JWT filter before username/password authentication

        return http.build();
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private CustomUserDetailsService userDetailsService;

    @Bean
    public AuthenticationManager authenticationManager(HttpSecurity http) throws Exception {
        AuthenticationManagerBuilder authenticationManagerBuilder =
                http.getSharedObject(AuthenticationManagerBuilder.class);

        authenticationManagerBuilder
                .userDetailsService(userDetailsService) // Custom userDetailsService
                .passwordEncoder(passwordEncoder()); // Custom password encoder

        return authenticationManagerBuilder.build();
    }

//    @Configuration
//    public class WebConfig implements WebMvcConfigurer {
//        @Override
//        public void addCorsMappings(CorsRegistry registry) {
//            registry.addMapping("**")
//                    .allowedOrigins("local") //on autorise toute origine, une fois que tout sera teste on mettra uniquement
//                                         //l'adresse du serveur React
//                    .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
//                    .allowedHeaders("*")
//                    .allowCredentials(true);
//        }
//    }

    @Configuration
    public static class WebConfig {

        @Bean
        public FilterRegistrationBean<CustomCorsFilter> corsFilterRegistration() {
            FilterRegistrationBean<CustomCorsFilter> registrationBean = new FilterRegistrationBean<>();
            registrationBean.setFilter(new CustomCorsFilter());
            registrationBean.addUrlPatterns("/*"); // Appliquer le filtre à toutes les routes
            registrationBean.setOrder(0); // Définir la priorité du filtre
            return registrationBean;
        }
    }
}
