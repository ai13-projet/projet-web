package com.example.AI13_Projet.config;

import com.example.AI13_Projet.dto.QuestionDTO;
import com.example.AI13_Projet.dto.UserDTO;
import com.example.AI13_Projet.model.Question;
import com.example.AI13_Projet.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();

        // Explicit mapping for Question and QuestionDTO
        modelMapper.typeMap(QuestionDTO.class, Question.class)
                .addMappings(mapper -> mapper.map(QuestionDTO::getStatus, Question::setStatus));
        modelMapper.typeMap(Question.class, QuestionDTO.class)
                .addMappings(mapper -> mapper.map(Question::isActive, QuestionDTO::setStatus));

        modelMapper.typeMap(User.class, UserDTO.class).addMappings(mapper ->
                mapper.map(User::getRole, UserDTO::setRole)
        );

        return modelMapper;
    }
}
