package com.example.AI13_Projet.repository;

import com.example.AI13_Projet.model.RecordDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface RecordDetailRepository extends JpaRepository<RecordDetail, Long> {
    List<RecordDetail> findByAnswerId(Long answerId);
    // Custom JPQL query to find all RecordDetails by User ID through Record -> User relationship
    @Query("SELECT rd FROM RecordDetail rd WHERE rd.record.user.id = :userId")
    List<RecordDetail> findByUserId(Long userId);
    List<RecordDetail> findByQuestionId(Long questionId);
    @Query("SELECT rd FROM RecordDetail rd WHERE rd.record.quiz.id = :quizId")
    List<RecordDetail> findByQuizId(Long quizId);
}
