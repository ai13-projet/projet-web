package com.example.AI13_Projet.repository;

import com.example.AI13_Projet.model.Quiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface QuizRepository extends JpaRepository<Quiz, Long> {
    List<Quiz> findByCreatorId(Long creatorId);
    // Trouver toutes les versions d’un Quiz par titre et thème
    List<Quiz> findByTitleAndThemeLabel(String title, String themeLabel);

    // Trouver la version la plus récente d’un Quiz
    Optional<Quiz> findTopByTitleAndThemeLabelOrderByVersionDesc(String title, String themeLabel);
}
