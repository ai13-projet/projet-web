package com.example.AI13_Projet.repository;

import com.example.AI13_Projet.model.Theme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ThemeRepository extends JpaRepository<Theme, Long> {
    Optional<Theme> findByLabel(String label);
    boolean existsByLabel(String label);
}
