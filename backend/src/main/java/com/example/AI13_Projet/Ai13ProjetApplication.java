package com.example.AI13_Projet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ai13ProjetApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ai13ProjetApplication.class, args);
	}

}
