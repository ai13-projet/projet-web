package com.example.AI13_Projet.service;

import java.util.List;
import java.util.stream.Collectors;

import com.example.AI13_Projet.exception.ResourceNotFoundException;
import com.example.AI13_Projet.model.RecordDetail;
import com.example.AI13_Projet.repository.RecordDetailRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.AI13_Projet.dto.UserDTO;
import com.example.AI13_Projet.model.User;
import com.example.AI13_Projet.repository.UserRepository;

import jakarta.transaction.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.example.AI13_Projet.utils.PasswordGenerator;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private EmailService emailService;

    @Autowired
    private RecordDetailRepository recordDetailRepository;

    // Récupérer tous les utilisateurs sous forme de DTO
    public List<UserDTO> getAllUsers() {
        List<User> users = userRepository.findAll();
        return users.stream()
                .map(user -> modelMapper.map(user, UserDTO.class))
                .collect(Collectors.toList());
    }

    // Ajouter un nouvel utilisateur
    public UserDTO createUser(UserDTO userDTO) {
        // Validate fields are not null or empty
        if (!StringUtils.hasText(userDTO.getFirstname()) ||
                !StringUtils.hasText(userDTO.getLastname()) ||
                !StringUtils.hasText(userDTO.getEmail()) ||
                !StringUtils.hasText(userDTO.getCompany()) ||
                !StringUtils.hasText(userDTO.getPhone()) ||
                !StringUtils.hasText(userDTO.getRole())){
            throw new IllegalArgumentException("All fields must be filled");
        }

        if (userDTO.getRole() == null || userDTO.getRole().isEmpty()) {
            throw new IllegalArgumentException("Role must not be null or empty.");
        }

        if(!userDTO.isValidRole()){
            throw new IllegalArgumentException("Invalid role: " + userDTO.getRole());
        }


        // Check if email is unique
        if (userRepository.existsByEmail(userDTO.getEmail())) {
            throw new IllegalArgumentException("A user with this email already exists: " + userDTO.getEmail());
        }

        // Generate a secure password
        String password = PasswordGenerator.generatePassword(24);  // Adjust length as needed

        // Hash the password before saving to the database (using BCrypt)
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);

        // Map DTO to entity and set the hashed password
        User user = modelMapper.map(userDTO, User.class);
        user.setPassword(hashedPassword);  // Set the hashed password

        // Save the user
        User savedUser = userRepository.save(user);

        // Send the email to the new user with their details
        String subject = "Welcome to AI13 Projet!";
        String message = "Hello " + userDTO.getFirstname() + " " + userDTO.getLastname() + ",\n\n" +
                "Your account has been created successfully. You were assigned the "+userDTO.getRole()+" role.\n"+
                "Here are your credentials:\n" +
                "   Login: " + userDTO.getEmail() + "\n" +
                "   Password: " + password + "\n\n" +
                "Best regards,\nAI13 Project Team";

        // Send the email with the password and email
        emailService.sendEmail(userDTO.getEmail(), subject, message);

        return modelMapper.map(savedUser, UserDTO.class);

    }

    // Récupérer un utilisateur par ID
    public UserDTO getUserById(Long id) {
        return userRepository.findById(id)
                .map(user -> modelMapper.map(user, UserDTO.class))
                .orElseThrow(() -> new ResourceNotFoundException("User not found with id: " + id));
    }

    public UserDTO updateUser(Long id, UserDTO userDTO) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("User not found with id: " + id));

        // Vérifier si un autre utilisateur utilise déjà cet e-mail
        if (userDTO.getEmail() != null && !userDTO.getEmail().equals(user.getEmail())) {
            if (userRepository.existsByEmail(userDTO.getEmail())) {
                throw new IllegalArgumentException("A user with this email already exists: " + userDTO.getEmail());
            }
            user.setEmail(userDTO.getEmail());
        }

        // Mettre à jour les champs fournis
        if (userDTO.getFirstname() != null) {
            user.setFirstname(userDTO.getFirstname());
        }
        if (userDTO.getLastname() != null) {
            user.setLastname(userDTO.getLastname());
        }
        if (userDTO.getPhone() != null) {
            user.setPhone(userDTO.getPhone());
        }
        if (userDTO.getCompany() != null) {
            user.setCompany(userDTO.getCompany());
        }

        if (userDTO.isActive() != user.isActive()) {
            user.setActive(userDTO.isActive());
        }

        User updatedUser = userRepository.save(user);
        return modelMapper.map(updatedUser, UserDTO.class);
    }

    @Transactional
    public void deleteUser(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found with id: " + userId));
        // Delete associated RecordDetail entities first
        List<RecordDetail> recordDetails = recordDetailRepository.findByUserId(userId);
        recordDetailRepository.deleteAll(recordDetails);

        // Supprime l'utilisateur
        userRepository.deleteById(userId);
    }

    // Activer ou désactiver un utilisateur
    public UserDTO updateUserStatus(Long id, boolean isActive) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User not found with id: " + id));

        user.setActive(isActive); // Mettre à jour l'état du compte
        User updatedUser = userRepository.save(user);

        return modelMapper.map(updatedUser, UserDTO.class);
    }

    public boolean getUserStatus(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found with id: " + userId));
        return user.isActive();
    }
}
