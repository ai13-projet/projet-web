package com.example.AI13_Projet.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.example.AI13_Projet.repository.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.AI13_Projet.dto.RecordDTO;
import com.example.AI13_Projet.dto.RecordDetailDTO;
import com.example.AI13_Projet.exception.ResourceNotFoundException;
import com.example.AI13_Projet.model.Answer;
import com.example.AI13_Projet.model.Question;
import com.example.AI13_Projet.model.Quiz;
import com.example.AI13_Projet.model.Record;
import com.example.AI13_Projet.model.RecordDetail;
import com.example.AI13_Projet.model.User;

@Service
public class RecordService {

    @Autowired
    private RecordRepository recordRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private RecordDetailRepository recordDetailRepository;

    @Autowired
    private ModelMapper modelMapper;

    public RecordDTO createRecord(Long userId, Long quizId, RecordDTO recordDTO) {
        // Retrieve the User
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found with id: " + userId));

        // Vérifier si l'utilisateur est activé
        if (!user.isActive()) { // Si le statut est false
            throw new IllegalArgumentException("User with id " + userId + " is not active. Unable to create a new record.");
        }

        // Retrieve the Quiz
        Quiz quiz = quizRepository.findById(quizId)
                .orElseThrow(() -> new ResourceNotFoundException("Quiz not found with id: " + quizId));

        // Create a new Record
        Record record = new Record();
        record.setUser(user);
        record.setQuiz(quiz);
        record.setStartedAt(LocalDateTime.now());

        // Initialize details list if not present
        record.setDetails(new ArrayList<>());

        // Map and set details if provided
        if (recordDTO.getDetails() != null && !recordDTO.getDetails().isEmpty()) {
            List<RecordDetail> details = recordDTO.getDetails().stream()
                    .map(detailDTO -> {
                        RecordDetail detail = modelMapper.map(detailDTO, RecordDetail.class);
                        detail.setRecord(record); // Set parent record
                        return detail;
                    }).collect(Collectors.toList());
            record.setDetails(details);
        }

        // Save the Record
        Record savedRecord = recordRepository.save(record);

        // Return the saved Record as DTO
        return modelMapper.map(savedRecord, RecordDTO.class);
    }

    public List<RecordDTO> getRecordsByUser(Long userId) {
        List<Record> records = recordRepository.findByUserId(userId);
        return records.stream()
                .map(record -> modelMapper.map(record, RecordDTO.class))
                .collect(Collectors.toList());
    }

    public List<RecordDTO> getRecordsByQuiz(Long quizId) {
        List<Record> records = recordRepository.findByQuizId(quizId);
        return records.stream()
                .map(record -> modelMapper.map(record, RecordDTO.class))
                .collect(Collectors.toList());
    }

    public void deleteRecord(Long recordId) {
        if (!recordRepository.existsById(recordId)) {
            throw new ResourceNotFoundException("Record not found with id " + recordId);
        }
        recordRepository.deleteById(recordId);
    }

    public RecordDTO updateRecord(Long recordId, RecordDTO recordDTO) {
        // Find the record
        Record record = recordRepository.findById(recordId)
                .orElseThrow(() -> new ResourceNotFoundException("Record not found with id " + recordId));

        // Update only provided fields
        if (recordDTO.getScore() != null) {
            record.setScore(recordDTO.getScore());
        }
        if (recordDTO.getDuration() != null) {
            record.setDuration(recordDTO.getDuration());
        }
        if (recordDTO.getCompletedAt() != null) {
            record.setCompletedAt(recordDTO.getCompletedAt());
        }

        // Save the updated record
        record = recordRepository.save(record);

        // Convert and return the updated record as DTO
        return modelMapper.map(record, RecordDTO.class);
    }

    public RecordDTO addAnswersToRecord(Long recordId, List<RecordDetailDTO> detailsDTO) {
        // Retrieve the existing Record
        Record record = recordRepository.findById(recordId)
                .orElseThrow(() -> new ResourceNotFoundException("Record not found with id " + recordId));

        // Initialize the record details list if null
        if (record.getDetails() == null) {
            record.setDetails(new ArrayList<>());
        }

        // List to hold new RecordDetails
        List<RecordDetail> newDetails = new ArrayList<>();

        for (RecordDetailDTO detailDTO : detailsDTO) {
            // Retrieve Question and Answer
            Question question = questionRepository.findById(detailDTO.getQuestionId())
                    .orElseThrow(() -> new ResourceNotFoundException("Question not found with id " + detailDTO.getQuestionId()));

            Answer answer = answerRepository.findById(detailDTO.getAnswerId())
                    .orElseThrow(() -> new ResourceNotFoundException("Answer not found with id " + detailDTO.getAnswerId()));

            // Check if the RecordDetail already exists to avoid duplicates
            boolean exists = record.getDetails().stream().anyMatch(detail ->
                    detail.getQuestion().getId().equals(question.getId()));

            if (!exists) {
                // Create and add a new RecordDetail
                RecordDetail detail = new RecordDetail();
                detail.setRecord(record); // Link RecordDetail to Record
                detail.setQuestion(question); // Attach managed Question
                detail.setAnswer(answer); // Attach managed Answer
                detail.setCorrect(answer.isCorrect());
                newDetails.add(detail);
            }
        }

        // Save new details explicitly to avoid transient object issues
        recordDetailRepository.saveAll(newDetails);

        // Add the saved details to the existing record
        record.getDetails().addAll(newDetails);

        // Recalculate the score
        int updatedScore = (int) record.getDetails().stream().filter(RecordDetail::isCorrect).count();
        record.setScore(updatedScore);

        // Save the updated Record (no need to cascade here as details are already saved)
        Record updatedRecord = recordRepository.save(record);

        // Convert to DTO and return
        return modelMapper.map(updatedRecord, RecordDTO.class);
    }

    public RecordDTO getLastRecordByUserAndQuiz(Long userId, Long quizId) {
        // Vérification de l'existence de l'utilisateur et du quiz
        userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found with id: " + userId));
        quizRepository.findById(quizId)
                .orElseThrow(() -> new ResourceNotFoundException("Quiz not found with id: " + quizId));

        // Récupérer le dernier record
        Record lastRecord = recordRepository.findTopByUserIdAndQuizIdOrderByCompletedAtDesc(userId, quizId)
                .orElseThrow(() -> new ResourceNotFoundException("No records found for userId: " + userId + " and quizId: " + quizId));

        // Retourner le DTO
        return modelMapper.map(lastRecord, RecordDTO.class);
    }
}
