package com.example.AI13_Projet.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import java.util.Properties;
@Service
public class EmailService {

    private final JavaMailSender emailSender;

    @Value("${spring.mail.username}")
    private String username;

    public EmailService(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    // Method to send a simple email
    public void sendEmail(String to, String subject, String message) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setFrom(username); // Sender email
        email.setTo(to); // Recipient email
        email.setSubject(subject); // Subject of the email
        email.setText(message); // Message body

        emailSender.send(email);
    }
}