package com.example.AI13_Projet.service;

import com.example.AI13_Projet.dto.AnswerDTO;
import com.example.AI13_Projet.dto.QuestionDTO;
import com.example.AI13_Projet.exception.ResourceNotFoundException;
import com.example.AI13_Projet.model.Answer;
import com.example.AI13_Projet.model.Question;
import com.example.AI13_Projet.model.Quiz;
import com.example.AI13_Projet.repository.AnswerRepository;
import com.example.AI13_Projet.repository.QuestionRepository;
import com.example.AI13_Projet.repository.QuizRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class QuestionService {

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private ModelMapper modelMapper;

    // Ajouter une liste de questions à un quiz
    public List<QuestionDTO> addQuestionsToQuiz(Long quizId, List<QuestionDTO> questionsDTO) {
        // Récupérer le Quiz correspondant
        Quiz quiz = quizRepository.findById(quizId)
                .orElseThrow(() -> new ResourceNotFoundException("Quiz not found with id " + quizId));

        // Mapper les DTO vers des entités Question et les associer au Quiz
        List<Question> questions = questionsDTO.stream().map(questionDTO -> {
            Question question = modelMapper.map(questionDTO, Question.class);
            question.setQuiz(quiz);
            return question;
        }).collect(Collectors.toList());

        // Sauvegarder les Questions
        List<Question> savedQuestions = questionRepository.saveAll(questions);

        // Mettre à jour la liste des questions dans le Quiz
        quiz.getQuestions().addAll(savedQuestions);
        quizRepository.save(quiz);

        // Retourner les Questions sauvegardées sous forme de DTO
        return savedQuestions.stream()
                .map(savedQuestion -> modelMapper.map(savedQuestion, QuestionDTO.class))
                .collect(Collectors.toList());
    }

    // Récupérer toutes les questions d'un quiz
    public List<QuestionDTO> getQuestionsByQuiz(Long quizId) {
        Quiz quiz = quizRepository.findById(quizId)
                .orElseThrow(() -> new ResourceNotFoundException("Quiz not found with id " + quizId));

        List<Question> questions = questionRepository.findByQuizId(quiz.getId());
        return questions.stream()
                .map(question -> modelMapper.map(question, QuestionDTO.class))
                .toList();
    }

    public QuestionDTO updateQuestionPosition(Long questionId, int newPosition) {
        Question question = questionRepository.findById(questionId)
                .orElseThrow(() -> new ResourceNotFoundException("Question not found with id " + questionId));

        if (newPosition < 1) {
            throw new IllegalArgumentException("Position must be greater than 0");
        }

        // Find the question that currently has the target position
        Question existingQuestionAtPosition = questionRepository.findByPosition(newPosition)
                .orElse(null);

        // Swap positions if another question exists at the new position
        if (existingQuestionAtPosition != null) {
            int originalPosition = question.getPosition();
            existingQuestionAtPosition.setPosition(originalPosition);
            questionRepository.save(existingQuestionAtPosition);
        }

        question.setPosition(newPosition);
        Question updatedQuestion = questionRepository.save(question);

        return modelMapper.map(updatedQuestion, QuestionDTO.class);
    }

    public AnswerDTO updateAnswerPosition(Long answerId, int newPosition) {
        Answer answer = answerRepository.findById(answerId)
                .orElseThrow(() -> new ResourceNotFoundException("Answer not found with id " + answerId));

        if (newPosition < 1) {
            throw new IllegalArgumentException("Position must be greater than 0");
        }

        // Find the answer that currently has the target position
        Answer existingAnswerAtPosition = answerRepository.findByPosition(newPosition)
                .orElse(null);

        // Swap positions if another answer exists at the new position
        if (existingAnswerAtPosition != null) {
            int originalPosition = answer.getPosition();
            existingAnswerAtPosition.setPosition(originalPosition);
            answerRepository.save(existingAnswerAtPosition);
        }

        answer.setPosition(newPosition);
        Answer updatedAnswer = answerRepository.save(answer);

        return modelMapper.map(updatedAnswer, AnswerDTO.class);
    }
}
