package com.example.AI13_Projet.service;

import com.example.AI13_Projet.model.User;
import com.example.AI13_Projet.repository.UserRepository;
import com.example.AI13_Projet.utils.AuthenticationResponse;
import com.example.AI13_Projet.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    private final AuthenticationManager authenticationManager;
    private final JwtUtil jwtUtil;
    private final UserDetailsService userDetailsService;
    private final UserRepository userRepository;

    @Autowired
    public AuthService(AuthenticationManager authenticationManager, JwtUtil jwtUtil, UserDetailsService userDetailsService, UserRepository userRepository) {
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
        this.userDetailsService = userDetailsService;
        this.userRepository = userRepository;
    }
//
//    public String authenticate(String email, String password) {
//        try {
//            System.out.println("Attempting authentication for user: " + email);
//
//            // Perform authentication
//            Authentication authentication = authenticationManager.authenticate(
//                    new UsernamePasswordAuthenticationToken(email, password)
//            );
//
//            System.out.println("Authentication successful for user: " + email);
//
//            // Retrieve user role from the repository
//            String role = userRepository.getRoleByEmail(email);
//            System.out.println("Role: " + role);
//
//            if (role == null) {
//                throw new RuntimeException("Role not found for user: " + email);
//            }
//
//            // Generate JWT token
//            return jwtUtil.generateToken(email, role);
//
//        } catch (BadCredentialsException e) {
//            System.out.println("Invalid credentials provided for user: " + email);
//            throw new BadCredentialsException("Invalid email or password");
//        } catch (RuntimeException e) {
//            System.out.println("Authentication failed for user: " + email + " - " + e.getMessage());
//            throw e; // Re-throw custom or runtime exceptions
//        }
//    }

    public AuthenticationResponse authenticate(String email, String password) {
        try {
            System.out.println("Attempting authentication for user: " + email);

            // Perform authentication
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(email, password)
            );

            System.out.println("Authentication successful for user: " + email);

            // Retrieve user details from the repository
            int userId = userRepository.getIdByEmail(email); // Fetch user ID
            String role = userRepository.getRoleByEmail(email); // Fetch user role

            if (userId <=0) {
                throw new RuntimeException("User ID not found for user: " + email);
            }

            if (role == null) {
                throw new RuntimeException("Role not found for user: " + email);
            }

            System.out.println("User ID: " + userId + ", Role: " + role);

            // Generate JWT token
            String token = jwtUtil.generateToken(email, role);

            // Return structured response containing both userId and token
            return new AuthenticationResponse(userId, token);

        } catch (BadCredentialsException e) {
            System.out.println("Invalid credentials provided for user: " + email);
            throw new BadCredentialsException("Invalid email or password");
        } catch (RuntimeException e) {
            System.out.println("Authentication failed for user: " + email + " - " + e.getMessage());
            throw e; // Re-throw custom or runtime exceptions
        }
    }

}
