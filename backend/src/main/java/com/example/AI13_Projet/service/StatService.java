package com.example.AI13_Projet.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.AI13_Projet.exception.ResourceNotFoundException;

import com.example.AI13_Projet.dto.QuizStatDTO;
import com.example.AI13_Projet.dto.UserStatDTO;
import com.example.AI13_Projet.dto.QuizRankingDTO;

import com.example.AI13_Projet.model.Record;
import com.example.AI13_Projet.model.User;
import com.example.AI13_Projet.model.Quiz;

import com.example.AI13_Projet.repository.RecordRepository;
import com.example.AI13_Projet.repository.UserRepository;
import com.example.AI13_Projet.repository.QuizRepository;

import java.util.List;

@Service
public class StatService {

    @Autowired
    private RecordRepository recordRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private QuizRepository quizRepository;

    public QuizStatDTO getQuizStatistics(Long quizId) {
        // Récupérer le quiz pour vérifier son existence et obtenir son titre
        Quiz quiz = quizRepository.findById(quizId)
                .orElseThrow(() -> new ResourceNotFoundException("Quiz not found with id " + quizId));

        // Récupérer les records associés au quiz
        List<Record> records = recordRepository.findByQuizId(quizId);

        if (records.isEmpty()) {
            throw new ResourceNotFoundException("No records found for quiz with id " + quizId);
        }

        QuizStatDTO quizStatDTO = new QuizStatDTO();

        // Calcul des statistiques
        quizStatDTO.setQuizId(quizId);
        quizStatDTO.setQuizTitle(quiz.getTitle());

        double averageScore = records.stream().mapToInt(com.example.AI13_Projet.model.Record::getScore).average().orElse(0.0);
        quizStatDTO.setAverageScore(averageScore);

        double averageTime = records.stream().mapToInt(Record::getDuration).average().orElse(0.0);
        quizStatDTO.setAverageTime(averageTime);

        int numberOfRespondents = records.size();
        quizStatDTO.setNumberOfRespondents(numberOfRespondents);

        List<User> ranking = getUsersRankingOnRecords(records);
        quizStatDTO.setRanking(ranking);

        // Retourner les statistiques
        return quizStatDTO;
    }

    public UserStatDTO getUserStatistics(Long userId) {
        // Vérifier que l'utilisateur existe
        if (!userRepository.existsById(userId)) {
            throw new ResourceNotFoundException("User not found with id " + userId);
        }

        // Récupérer les records associés à l'utilisateur
        List<Record> records = recordRepository.findByUserId(userId);
        if (records.isEmpty()) {
            throw new ResourceNotFoundException("No records found for user with id " + userId);
        }

        // Initialiser le DTO pour les statistiques utilisateur
        UserStatDTO userStatDTO = new UserStatDTO();
        userStatDTO.setUserId(userId);

        // Calculer la moyenne des scores et du temps
        double averageScore = records.stream().mapToInt(Record::getScore).average().orElse(0.0);
        userStatDTO.setAverageScore(averageScore);

        double averageTime = records.stream().mapToInt(Record::getDuration).average().orElse(0.0);
        userStatDTO.setAverageTime(averageTime);

        // Calculer le nombre de quiz répondus
        int numberOfQuizzes = (int) records.stream()
                .map(record -> record.getQuiz().getId()) // Obtenir les IDs des quiz
                .distinct() // Supprimer les doublons
                .count();
        userStatDTO.setNumberOfQuizzes(numberOfQuizzes);

        // Calculer le classement pour chaque quiz auquel l'utilisateur a répondu
        List<QuizRankingDTO> rankings = records.stream().map(record -> {
            Quiz quiz = record.getQuiz();
            List<Record> allQuizRecords = recordRepository.findByQuizId(quiz.getId());
            QuizRankingDTO quizRankingDTO = new QuizRankingDTO();

            quizRankingDTO.setQuizId(quiz.getId());
            quizRankingDTO.setQuizTitle(quiz.getTitle());

            // Calculer le score/durée pour chaque utilisateur ayant répondu au quiz
            List<Double> scoreRatios = allQuizRecords.stream()
                    .filter(r -> r.getDuration() > 0) // Éviter division par zéro
                    .map(r -> r.getScore() / (double) r.getDuration())
                    .sorted((r1, r2) -> Double.compare(r2, r1)) // Trier en ordre décroissant
                    .toList();

            // Calculer le ratio de l'utilisateur pour ce quiz
            double userScoreRatio = record.getScore() / (double) record.getDuration();
            quizRankingDTO.setScoreRatio(userScoreRatio);

            // Calculer le rang de l'utilisateur
            int rank = scoreRatios.indexOf(userScoreRatio) + 1;
            quizRankingDTO.setRank(rank);

            // Retourner un DTO de classement pour ce quiz
            return quizRankingDTO;
        }).toList();

        // Ajouter les rankings au DTO utilisateur
        userStatDTO.setRankings(rankings);

        // Retourner les statistiques utilisateur avec rankings
        return userStatDTO;
    }


    public List<User> getUsersRankingOnRecords(List<Record> records) {
        return records.stream()
                .filter(record -> record.getDuration() > 0) // Filtrer les records avec une durée valide
                .sorted((r1, r2) -> Double.compare(
                        r2.getScore() / (double) r2.getDuration(), // Ratio de r2
                        r1.getScore() / (double) r1.getDuration()  // Ratio de r1
                ))
                .map(Record::getUser) // Extraire l'utilisateur associé à chaque record
                .distinct() // Éliminer les doublons (si un utilisateur a plusieurs records)
                .toList();
    }
}
