package com.example.AI13_Projet.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import com.example.AI13_Projet.dto.*;
import com.example.AI13_Projet.model.*;
import com.example.AI13_Projet.repository.*;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.AI13_Projet.exception.ResourceNotFoundException;
import com.example.AI13_Projet.repository.AnswerRepository;
import com.example.AI13_Projet.repository.QuestionRepository;
import com.example.AI13_Projet.repository.QuizRepository;
import com.example.AI13_Projet.repository.ThemeRepository;
import com.example.AI13_Projet.repository.UserRepository;

@Service
public class QuizService {

    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ThemeRepository themeRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private RecordDetailRepository recordDetailRepository;

    @Transactional
    public QuizDTO createOrUpdateQuizVersion(Long userId, QuizDTO quizDTO) {
        // Vérifier si l'utilisateur existe
        User creator = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found with id " + userId));

        // Vérifier si le thème existe
        Theme theme = themeRepository.findByLabel(quizDTO.getThemeLabel())
                .orElseThrow(() -> new ResourceNotFoundException("Theme not found with label " + quizDTO.getThemeLabel()));

        // Vérifier si un quiz existe déjà avec le même titre et thème
        List<Quiz> existingQuizzes = quizRepository.findByTitleAndThemeLabel(quizDTO.getTitle(), quizDTO.getThemeLabel());

        if (!existingQuizzes.isEmpty()) {
            // Une version existe déjà, créer une nouvelle version
            int newVersion = existingQuizzes.stream()
                    .mapToInt(Quiz::getVersion)
                    .max()
                    .orElse(0) + 1;

            Quiz newQuizVersion = new Quiz();
            newQuizVersion.setTitle(quizDTO.getTitle());
            newQuizVersion.setTheme(theme);
            newQuizVersion.setCreator(creator);
            newQuizVersion.setVersion(newVersion);
            newQuizVersion.setCreationDate(Timestamp.valueOf(LocalDateTime.now()));

            Quiz savedQuiz = quizRepository.save(newQuizVersion);
            return modelMapper.map(savedQuiz, QuizDTO.class);
        }

        // Si aucun quiz existant, créer un nouveau quiz de base
        Quiz quiz = new Quiz();
        quiz.setTitle(quizDTO.getTitle());
        quiz.setTheme(theme);
        quiz.setCreator(creator);
        quiz.setVersion(1); // Première version
        quiz.setCreationDate(Timestamp.valueOf(LocalDateTime.now()));

        Quiz savedQuiz = quizRepository.save(quiz);
        return modelMapper.map(savedQuiz, QuizDTO.class);
    }

    @Transactional
    public void deleteQuiz(Long quizId) {
        Quiz quiz = quizRepository.findById(quizId)
                .orElseThrow(() -> new ResourceNotFoundException("Quiz with ID " + quizId + " not found."));
        // Delete associated RecordDetail entities first
        List<RecordDetail> recordDetails = recordDetailRepository.findByQuizId(quizId);
        recordDetailRepository.deleteAll(recordDetails);

        quizRepository.delete(quiz);
    }

    public List<QuizDTO> getQuizzesCreatedByUser(Long userId) {
        List<Quiz> quizzes = quizRepository.findByCreatorId(userId);
        return quizzes.stream()
                .map(quiz -> modelMapper.map(quiz, QuizDTO.class))
                .collect(Collectors.toList());
    }

    // Récupérer tous les quizzes
    public List<QuizDTO> getAllQuizzes() {
        List<Quiz> quizzes = quizRepository.findAll();
        return quizzes.stream()
                .map(quiz -> modelMapper.map(quiz, QuizDTO.class))
                .collect(Collectors.toList());
    }

    // Récupérer les quizzes actifs
    public List<QuizDTO> getActiveQuizzes() {
        List<Quiz> activeQuizzes = quizRepository.findAll().stream()
                .filter(Quiz::isActive)
                .toList();

        // Convertir en DTO
        return activeQuizzes.stream()
                .map(quiz -> modelMapper.map(quiz, QuizDTO.class))
                .collect(Collectors.toList());
    }

    public List<QuizDTO> getAllVersions(Long quizId) {
        // Récupère le Quiz de base via son ID
        Quiz baseQuiz = quizRepository.findById(quizId)
                .orElseThrow(() -> new ResourceNotFoundException("Quiz not found with ID: " + quizId));

        // Recherche les versions en fonction du titre et du thème
        List<Quiz> versions = quizRepository.findByTitleAndThemeLabel(baseQuiz.getTitle(), baseQuiz.getTheme().getLabel());

        // Conversion en DTO
        return versions.stream()
                .map(quiz -> modelMapper.map(quiz, QuizDTO.class))
                .collect(Collectors.toList());
    }

    public QuizDTO getLatestVersion(Long quizId) {
        // Récupère le Quiz de base via son ID
        Quiz baseQuiz = quizRepository.findById(quizId)
                .orElseThrow(() -> new ResourceNotFoundException("Quiz not found with ID: " + quizId));

        // Recherche la version la plus récente en fonction du titre et du thème
        Quiz latestVersion = quizRepository.findTopByTitleAndThemeLabelOrderByVersionDesc(
                        baseQuiz.getTitle(), baseQuiz.getTheme().getLabel())
                .orElseThrow(() -> new ResourceNotFoundException(
                        "No versions found for title: " + baseQuiz.getTitle() + " and theme: " + baseQuiz.getTheme().getLabel()));

        return modelMapper.map(latestVersion, QuizDTO.class);
    }

    // Create a new theme with unique label
    public ThemeDTO createTheme(ThemeDTO themeDTO) {
        // Check if a theme with the same label already exists
        boolean exists = themeRepository.existsByLabel(themeDTO.getLabel());
        if (exists) {
            throw new IllegalArgumentException("A theme with the label '" + themeDTO.getLabel() + "' already exists.");
        }

        // Map DTO to entity
        Theme theme = modelMapper.map(themeDTO, Theme.class);

        // Save the theme
        Theme savedTheme = themeRepository.save(theme);

        // Map entity to DTO and return
        return modelMapper.map(savedTheme, ThemeDTO.class);
    }

    // Update an existing theme with unique label check
    public ThemeDTO updateTheme(Long themeId, ThemeDTO themeDTO) {
        Theme existingTheme = themeRepository.findById(themeId)
                .orElseThrow(() -> new ResourceNotFoundException("Theme not found with id " + themeId));

        // If the label is being updated, check for uniqueness
        if (!existingTheme.getLabel().equals(themeDTO.getLabel())) {
            themeRepository.findByLabel(themeDTO.getLabel())
                    .ifPresent(duplicateTheme -> {
                        throw new IllegalArgumentException("A theme with the label '" + themeDTO.getLabel() + "' already exists.");
                    });
            existingTheme.setLabel(themeDTO.getLabel());
        }

        Theme updatedTheme = themeRepository.save(existingTheme);
        return modelMapper.map(updatedTheme, ThemeDTO.class);
    }

    public List<QuizDTO> getQuizzesByTheme(Long themeId) {
        Theme theme = themeRepository.findById(themeId)
                .orElseThrow(() -> new ResourceNotFoundException("Theme not found with id " + themeId));

        // Map the quizzes to QuizDTO
        return theme.getQuizzes().stream()
                .map(quiz -> modelMapper.map(quiz, QuizDTO.class))
                .collect(Collectors.toList());
    }

    public QuizDTO updateQuizStatus(Long quizId, boolean status) {
        Quiz quiz = quizRepository.findById(quizId)
                .orElseThrow(() -> new ResourceNotFoundException("Quiz not found with id " + quizId));

        quiz.setActive(status);

        Quiz updatedQuiz = quizRepository.save(quiz);

        return new ModelMapper().map(updatedQuiz, QuizDTO.class);
    }

    public void deleteTheme(Long themeId) {
        if (!themeRepository.existsById(themeId)) {
            throw new ResourceNotFoundException("Theme not found with id " + themeId);
        }
        themeRepository.deleteById(themeId);
    }

    public List<Theme> getAllThemes() {
        return themeRepository.findAll().stream()
                .map(theme -> modelMapper.map(theme, Theme.class))
                .collect(Collectors.toList());
    }

    // Ajouter une question
    public QuestionDTO addQuestion(QuestionDTO questionDTO) {
        Question question = modelMapper.map(questionDTO, Question.class);
        Question savedQuestion = questionRepository.save(question);
        return modelMapper.map(savedQuestion, QuestionDTO.class);
    }

    // Modifier une question
    public QuestionDTO updateQuestion(Long id, QuestionDTO questionDTO) {
        Question existingQuestion = questionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Question not found with id " + id));

        // Update only provided fields
        if (questionDTO.getLabel() != null) {
            existingQuestion.setLabel(questionDTO.getLabel());
        }
        if (questionDTO.getPosition() != null) {
            existingQuestion.setPosition(questionDTO.getPosition());
        }
        if (questionDTO.getStatus() != null) {
            existingQuestion.setStatus(questionDTO.getStatus());
        }

        Question updatedQuestion = questionRepository.save(existingQuestion);
        return modelMapper.map(updatedQuestion, QuestionDTO.class);
    }


    // Supprimer une question
    public void deleteQuestion(Long id) {
        // Delete associated RecordDetail entities first
        List<RecordDetail> recordDetails = recordDetailRepository.findByQuestionId(id);
        recordDetailRepository.deleteAll(recordDetails);

        questionRepository.deleteById(id);
    }

    // Ajouter une réponse
    public AnswerDTO addAnswer(AnswerDTO answerDTO) {
        // Vérifier que la question existe avant d'ajouter une réponse
        Question question = questionRepository.findById(answerDTO.getQuestionId())
                .orElseThrow(() -> new ResourceNotFoundException("Question not found with id " + answerDTO.getQuestionId()));

        // Convertir le DTO en entité
        Answer answer = modelMapper.map(answerDTO, Answer.class);
        answer.setQuestion(question);

        // Sauvegarder la réponse
        Answer savedAnswer = answerRepository.save(answer);

        // Retourner un DTO
        return modelMapper.map(savedAnswer, AnswerDTO.class);
    }

    // Modifier une réponse
    public AnswerDTO updateAnswer(Long id, AnswerDTO answerDTO) {
        Answer existingAnswer = answerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Answer not found with id " + id));

        // Update only provided fields
        if (answerDTO.getLabel() != null) {
            existingAnswer.setLabel(answerDTO.getLabel());
        }
        if (answerDTO.isCorrect() != null) {
            existingAnswer.setCorrect(answerDTO.isCorrect());
        }
        if (answerDTO.getPosition() != null) {
            existingAnswer.setPosition(answerDTO.getPosition());
        }
        if (answerDTO.isActive() != null) {
            existingAnswer.setActive(answerDTO.isActive());
        }

        Answer updatedAnswer = answerRepository.save(existingAnswer);
        return modelMapper.map(updatedAnswer, AnswerDTO.class);
    }

    // Supprimer une réponse
    @Transactional
    public void deleteAnswer(Long answerId) {
        // Delete associated RecordDetail entities first
        List<RecordDetail> recordDetails = recordDetailRepository.findByAnswerId(answerId);
        recordDetailRepository.deleteAll(recordDetails);

        // Ensuite, supprime l'Answer
        answerRepository.deleteById(answerId);
    }

    // Ajouter ou associer plusieurs réponses à une question
    public List<AnswerDTO> addAnswersToQuestion(Long questionId, List<AnswerDTO> answersDTO) {
        Question question = questionRepository.findById(questionId)
                .orElseThrow(() -> new ResourceNotFoundException("Question not found with id: " + questionId));

        if (answersDTO == null || answersDTO.isEmpty()) {
            throw new IllegalArgumentException("Answers list cannot be null or empty.");
        }

        // Check if the question already has a correct answer
        boolean existingCorrectAnswer = question.getCorrectAnswer() != null;
        if (existingCorrectAnswer && answersDTO.stream().anyMatch(AnswerDTO::isCorrect)) {
            throw new IllegalArgumentException("This question already has a correct answer. Only one correct answer is allowed.");
        }

        // Check if the new answers list contains multiple correct answers
        long newCorrectAnswerCount = answersDTO.stream().filter(AnswerDTO::isCorrect).count();
        if (newCorrectAnswerCount > 1) {
            throw new IllegalArgumentException("A question can only have one correct answer.");
        }

        List<Answer> answers = answersDTO.stream()
                .map(answerDTO -> {
                    Answer answer = modelMapper.map(answerDTO, Answer.class);
                    answer.setQuestion(question);
                    return answer;
                })
                .collect(Collectors.toList());

        List<Answer> savedAnswers = answerRepository.saveAll(answers);

        // Check if any of the newly added answers is correct
        boolean hasCorrectAnswer = existingCorrectAnswer || savedAnswers.stream().anyMatch(Answer::isCorrect);
        Answer correctAnswer = question.getCorrectAnswer() != null
                ? question.getCorrectAnswer()
                : savedAnswers.stream().filter(Answer::isCorrect).findFirst().orElse(null);

        // Update the question with its status and correct answer
        question.setStatus(hasCorrectAnswer);
        question.setCorrectAnswer(correctAnswer);
        questionRepository.save(question);

        // Return saved answers as DTOs
        return savedAnswers.stream()
                .map(answer -> modelMapper.map(answer, AnswerDTO.class))
                .collect(Collectors.toList());
    }

    // Récupérer toutes les réponses associées à une question
    public List<AnswerDTO> getAnswersByQuestionId(Long questionId) {
        List<Answer> answers = answerRepository.findByQuestionId(questionId);
        return answers.stream().map(answer -> modelMapper.map(answer, AnswerDTO.class)).collect(Collectors.toList());
    }

    // Activer ou désactiver une réponse
    public AnswerDTO updateAnswerStatus(Long id, boolean isActive) {
        Answer answer = answerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Answer not found with id: " + id));

        answer.setActive(isActive); // Mettre à jour l'état du compte
        Answer updatedAnswer = answerRepository.save(answer);

        return modelMapper.map(updatedAnswer, AnswerDTO.class);
    }

    // Activer ou désactiver une question
    public QuestionDTO updateQuestionStatus(Long id, boolean isActive) {
        Question question = questionRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Question not found with id: " + id));

        question.setStatus(isActive); // Mettre à jour l'état de la question
        Question updatedQuestion = questionRepository.save(question);

        return modelMapper.map(updatedQuestion, QuestionDTO.class);
    }
}
