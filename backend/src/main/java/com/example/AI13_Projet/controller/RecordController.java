package com.example.AI13_Projet.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.AI13_Projet.dto.RecordDTO;
import com.example.AI13_Projet.dto.RecordDetailDTO;
import com.example.AI13_Projet.exception.ResourceNotFoundException;
import com.example.AI13_Projet.service.RecordService;

@RestController
@RequestMapping("/records")
public class RecordController {

    @Autowired
    private RecordService recordService;

    @PostMapping("/users/{userId}/quizzes/{quizId}")
    public ResponseEntity<?> createRecord(@PathVariable Long userId, @PathVariable Long quizId, @RequestBody RecordDTO recordDTO) {
        try {
            RecordDTO createdRecord = recordService.createRecord(userId, quizId, recordDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(Map.of(
                    "message", "Record successfully created.",
                    "record", createdRecord
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "error", e.getMessage()
            ));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Map.of(
                    "error", e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "error", "An unexpected error occurred.",
                    "details", e.getMessage()
            ));
        }
    }

    @GetMapping("/users/{userId}")
    public ResponseEntity<?> getRecordsByUser(@PathVariable Long userId) {
        try {
            List<RecordDTO> userRecords = recordService.getRecordsByUser(userId);
            return ResponseEntity.ok(Map.of(
                    "message", "User records retrieved successfully.",
                    "user records", userRecords
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "error", e.getMessage()
            ));
        }
    }

    @GetMapping("/quizzes/{quizId}")
    public ResponseEntity<?> getRecordsByQuiz(@PathVariable Long quizId) {
        try {
            List<RecordDTO> quizRecords = recordService.getRecordsByQuiz(quizId);
            return ResponseEntity.ok(Map.of(
                    "message", "Quiz records retrieved successfully.",
                    "quiz records", quizRecords
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "error", e.getMessage()
            ));
        }
    }

    @PutMapping("/{recordId}")
    public ResponseEntity<?> updateRecord(@PathVariable Long recordId, @RequestBody RecordDTO recordDTO) {
        try {
            RecordDTO updatedRecord = recordService.updateRecord(recordId, recordDTO);
            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "Record successfully updated.",
                    "record", updatedRecord
            ));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(Map.of(
                    "status", "error",
                    "message", "Validation error: " + e.getMessage()
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", "Record not found: " + e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred.",
                    "details", e.getMessage()
            ));
        }
    }


    @DeleteMapping("/{recordId}/delete")
    public ResponseEntity<?> deleteRecord(@PathVariable Long recordId) {
        try {
            recordService.deleteRecord(recordId);
            return ResponseEntity.ok(Map.of(
                    "message", "Record successfully deleted."
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "error", e.getMessage()
            ));
        }
    }

    @PostMapping("/{recordId}/answers")
    public ResponseEntity<?> addAnswersToRecord(@PathVariable Long recordId, @RequestBody List<RecordDetailDTO> detailsDTO) {
        try {
            RecordDTO updatedRecord = recordService.addAnswersToRecord(recordId, detailsDTO);
            return ResponseEntity.ok(Map.of(
                    "message", "Answers successfully added to the record.",
                    "record", updatedRecord
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "error", e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "error", "An unexpected error occurred.",
                    "details", e.getMessage()
            ));
        }
    }

    @GetMapping("/users/{userId}/quizzes/{quizId}/last")
    public ResponseEntity<?> getLastRecordByUserAndQuiz(@PathVariable Long userId, @PathVariable Long quizId) {
        try {
            RecordDTO lastRecord = recordService.getLastRecordByUserAndQuiz(userId, quizId);
            return ResponseEntity.ok(Map.of(
                    "message", "Last record retrieved successfully.",
                    "record", lastRecord
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "error", e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "error", "An unexpected error occurred.",
                    "details", e.getMessage()
            ));
        }
    }
}
