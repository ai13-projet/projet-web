package com.example.AI13_Projet.controller;

import com.example.AI13_Projet.dto.UserStatDTO;
import com.example.AI13_Projet.dto.UserDTO;
import com.example.AI13_Projet.service.StatService;
import com.example.AI13_Projet.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.AI13_Projet.exception.ResourceNotFoundException;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private StatService statService;

    // Récupérer tous les utilisateurs
    @GetMapping("/all")
    public ResponseEntity<?> getAllUsers() {
        List<UserDTO> users = userService.getAllUsers();
        return ResponseEntity.ok(Map.of(
                "message", "All users retrieved successfully.",
                "users", users
        ));
    }

    // Ajouter un utilisateur
    @PostMapping
    public ResponseEntity<?> createUser(@RequestBody UserDTO userDTO) {
        try {
            UserDTO createdUser = userService.createUser(userDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(Map.of(
                    "message", "User successfully created.",
                    "user", createdUser
            ));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(Map.of(
                    "error", e.getMessage()
            ));
        }
    }

    // Récupérer un utilisateur par ID
    @GetMapping("/{id}")
    public ResponseEntity<?> getUserById(@PathVariable Long id) {
        try {
            UserDTO user = userService.getUserById(id);
            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "User retrieved successfully.",
                    "user", user
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", e.getMessage()
            ));
        }
    }

    // Mettre à jour un utilisateur
    @PutMapping("/{id}")
    public ResponseEntity<?> updateUser(@PathVariable Long id, @RequestBody UserDTO userDTO) {
        try {
            UserDTO updatedUser = userService.updateUser(id, userDTO);
            return ResponseEntity.ok(Map.of(
                    "message", "User successfully updated.",
                    "user", updatedUser
            ));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(Map.of(
                    "error", e.getMessage()
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "error", e.getMessage()
            ));
        }
    }

    // Supprimer un utilisateur
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable Long id) {
        try {
            userService.deleteUser(id);
            return ResponseEntity.ok(Map.of(
                    "message", "User successfully deleted."
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "error", e.getMessage()
            ));
        }
    }

    // Activer ou désactiver un utilisateur
    @PutMapping("/{id}/status")
    public ResponseEntity<?> updateUserStatus(@PathVariable Long id, @RequestParam boolean isActive) {
        try {
            UserDTO updatedUser = userService.updateUserStatus(id, isActive);
            String statusMessage = isActive ? "activated" : "deactivated";
            return ResponseEntity.ok(Map.of(
                    "message", "User account successfully " + statusMessage + ".",
                    "user", updatedUser
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of("error", e.getMessage()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "error", "An unexpected error occurred.",
                    "details", e.getMessage()
            ));
        }
    }

    @GetMapping("/{userId}/status")
    public ResponseEntity<?> getUserStatus(@PathVariable Long userId) {
        try {
            boolean status = userService.getUserStatus(userId);
            return ResponseEntity.ok(Map.of(
                    "message", "User status retrieved successfully.",
                    "status", status
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "error", e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "error", "An unexpected error occurred.",
                    "details", e.getMessage()
            ));
        }
    }

    @GetMapping("/{userId}/stats")
    public ResponseEntity<?> getUserStatistics(@PathVariable Long userId) {
        try {
            UserStatDTO stats = statService.getUserStatistics(userId);

            if (stats == null) {
                return ResponseEntity.ok(Map.of(
                        "status", "info",
                        "message", "No statistics found for the given user."
                ));
            }

            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "User statistics retrieved successfully.",
                    "data", stats
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", "User not found: " + e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred while retrieving user statistics.",
                    "details", e.getMessage()
            ));
        }
    }
}
