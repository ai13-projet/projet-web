package com.example.AI13_Projet.controller;

import com.example.AI13_Projet.dto.QuizDTO;
import com.example.AI13_Projet.dto.ThemeDTO;
import com.example.AI13_Projet.exception.ResourceNotFoundException;
import com.example.AI13_Projet.service.QuizService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/themes")
public class ThemeController {

    @Autowired
    private QuizService quizService;

    @Autowired
    private ModelMapper modelMapper;

    // Create a theme
    @PostMapping
    public ResponseEntity<?> createTheme(@RequestBody ThemeDTO theme) {
        try {
            ThemeDTO createdTheme = quizService.createTheme(theme);
            return ResponseEntity.status(HttpStatus.CREATED).body(Map.of(
                    "status", "success",
                    "message", "Theme created successfully.",
                    "data", createdTheme
            ));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Map.of(
                    "status", "error",
                    "message", e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred while creating the theme.",
                    "details", e.getMessage()
            ));
        }
    }

    // Update a theme
    @PutMapping("/{themeId}")
    public ResponseEntity<?> updateTheme(@PathVariable Long themeId, @RequestBody ThemeDTO theme) {
        try {
            ThemeDTO updatedTheme = quizService.updateTheme(themeId, theme);
            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "Theme updated successfully.",
                    "data", updatedTheme
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", "Theme not found: " + e.getMessage()
            ));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Map.of(
                    "status", "error",
                    "message", e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred while updating the theme.",
                    "details", e.getMessage()
            ));
        }
    }

    // Supprimer un thème
    @DeleteMapping("/{themeId}")
    public ResponseEntity<?> deleteTheme(@PathVariable Long themeId) {
        try {
            quizService.deleteTheme(themeId);
            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "Theme deleted successfully."
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", "Theme not found: " + e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred while deleting the theme.",
                    "details", e.getMessage()
            ));
        }
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllThemesAsDTO() {
        try {
            List<ThemeDTO> themes = quizService.getAllThemes().stream()
                    .map(theme -> modelMapper.map(theme, ThemeDTO.class))
                    .toList();
            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "Themes retrieved successfully.",
                    "data", themes
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred while retrieving themes.",
                    "details", e.getMessage()
            ));
        }
    }

    // Endpoint pour récupérer tous les quiz associés à un thème
    @GetMapping("/{themeId}/quizzes")
    public ResponseEntity<?> getQuizzesByTheme(@PathVariable Long themeId) {
        try {
            List<QuizDTO> quizzes = quizService.getQuizzesByTheme(themeId);
            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "Quizzes retrieved successfully.",
                    "data", quizzes
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(404).body(Map.of(
                    "status", "error",
                    "message", e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(500).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred.",
                    "details", e.getMessage()
            ));
        }
    }

}