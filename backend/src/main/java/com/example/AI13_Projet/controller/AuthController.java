package com.example.AI13_Projet.controller;

import com.example.AI13_Projet.service.AuthService;
import com.example.AI13_Projet.utils.AuthenticationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginRequest loginRequest) {
        System.out.println("In login controller");

        // Input validation
        if (loginRequest.getEmail() == null || loginRequest.getPassword() == null) {
            return ResponseEntity.badRequest().body(Map.of(
                    "status", "error",
                    "message", "Email and password must not be null."
            ));
        }

        try {
            // Attempt authentication
            System.out.println("Attempting Authentication for: " + loginRequest.getEmail());
            AuthenticationResponse authResp = authService.authenticate(loginRequest.getEmail(), loginRequest.getPassword());

            // Return success response
            System.out.println("Authentication successful for user: " + loginRequest.getEmail());
            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "Login successful.",
                    "token", authResp.getToken(),
                    "id", authResp.getUserId()
            ));

        } catch (RuntimeException e) {
            // Log error and return failure response
            System.out.println("Authentication failed for user: " + loginRequest.getEmail() + " - " + e.getMessage());
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(Map.of(
                    "status", "error",
                    "message", "Authentication failed. Invalid email or password.",
                    "details", e.getMessage()
            ));
        }
    }

    // Login request DTO
    public static class LoginRequest {
        private String email;
        private String password;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    // Token response DTO (Optional, if you want to standardize response handling)
    public static class AuthResponse {
        private String token;

        public AuthResponse(String token) {
            this.token = token;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }
}
