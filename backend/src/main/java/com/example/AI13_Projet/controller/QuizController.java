package com.example.AI13_Projet.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.AI13_Projet.dto.QuizDTO;
import com.example.AI13_Projet.dto.QuizStatDTO;
import com.example.AI13_Projet.exception.ResourceNotFoundException;
import com.example.AI13_Projet.service.QuizService;
import com.example.AI13_Projet.service.StatService;

@RestController
@RequestMapping("/quizzes")
public class QuizController {

    @Autowired
    private QuizService quizService;

    @Autowired
    private StatService statService;

    @PostMapping("/user/{userId}")
    public ResponseEntity<?> createQuiz(@PathVariable Long userId, @RequestBody QuizDTO quizDTO) {
        try {
            QuizDTO createdQuiz = quizService.createOrUpdateQuizVersion(userId, quizDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(Map.of(
                    "status", "success",
                    "message", "Quiz created or updated successfully.",
                    "data", createdQuiz
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred while creating/updating the quiz.",
                    "details", e.getMessage()
            ));
        }
    }

    //Récupérer tous les quizzes
    @GetMapping()
    public ResponseEntity<?> getAllQuizzes() {
        List<QuizDTO> quizzes = quizService.getAllQuizzes();
        return ResponseEntity.ok(Map.of(
                "message", "All quizzes retrieved successfully.",
                "data", quizzes
        ));
    }

    @GetMapping("/active")
    public ResponseEntity<?> getActiveQuizzes() {
        try {
            List<QuizDTO> activeQuizzes = quizService.getActiveQuizzes();
            if (activeQuizzes.isEmpty()) {
                return ResponseEntity.ok(Map.of(
                        "status", "info",
                        "message", "No active quizzes found."
                ));
            }
            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "Active quizzes retrieved successfully.",
                    "data", activeQuizzes
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred while retrieving active quizzes.",
                    "details", e.getMessage()
            ));
        }
    }

    // Activer/Désactiver un quiz
    @PutMapping("/{quizId}/status")
    public ResponseEntity<?> updateQuizStatus(@PathVariable Long quizId, @RequestParam boolean status) {
        try {
            QuizDTO updatedQuiz = quizService.updateQuizStatus(quizId, status);
            String statusMessage = status ? "activated" : "deactivated";
            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "Quiz successfully " + statusMessage + ".",
                    "data", updatedQuiz
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred while updating the quiz status.",
                    "details", e.getMessage()
            ));
        }
    }

    @DeleteMapping("/{quizId}")
    public ResponseEntity<?> deleteQuiz(@PathVariable Long quizId) {
        try {
            quizService.deleteQuiz(quizId);
            return ResponseEntity.status(HttpStatus.OK).body(Map.of(
                    "status", "success",
                    "message", "Quiz deleted successfully."
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", "Quiz not found: " + e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred while deleting the quiz.",
                    "details", e.getMessage()
            ));
        }
    }

    @GetMapping("/user/{userId}/created")
    public ResponseEntity<?> getQuizzesCreatedByUser(@PathVariable Long userId) {
        try {
            List<QuizDTO> quizzes = quizService.getQuizzesCreatedByUser(userId);
            if (quizzes.isEmpty()) {
                return ResponseEntity.ok(Map.of(
                        "status", "info",
                        "message", "No quizzes found for the given user."
                ));
            }
            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "Quizzes retrieved successfully.",
                    "data", quizzes
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", "User not found: " + e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred while retrieving quizzes.",
                    "details", e.getMessage()
            ));
        }
    }

    @GetMapping("/{quizId}/stats")
    public ResponseEntity<?> getQuizStatistics(@PathVariable Long quizId) {
        try {
            QuizStatDTO stats = statService.getQuizStatistics(quizId);
            if (stats == null) {
                return ResponseEntity.ok(Map.of(
                        "status", "info",
                        "message", "No statistics found for the given quiz."
                ));
            }
            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "Quiz statistics retrieved successfully.",
                    "data", stats
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", "Quiz not found: " + e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred while retrieving quiz statistics.",
                    "details", e.getMessage()
            ));
        }
    }

    // Endpoint pour récupérer toutes les versions d’un Quiz donné via son ID
    @GetMapping("/{quizId}/versions")
    public ResponseEntity<?> getAllVersions(@PathVariable Long quizId) {
        List<QuizDTO> versions = quizService.getAllVersions(quizId);
        return ResponseEntity.ok(Map.of(
                "status", "success",
                "message", "All versions retrieved successfully.",
                "data", versions
        ));
    }

    // Endpoint pour récupérer la version la plus récente d’un Quiz donné via son ID
    @GetMapping("/{quizId}/latest")
    public ResponseEntity<?> getLatestVersion(@PathVariable Long quizId) {
        QuizDTO latestVersion = quizService.getLatestVersion(quizId);
        return ResponseEntity.ok(Map.of(
                "status", "success",
                "message", "Latest version retrieved successfully.",
                "data", latestVersion
        ));
    }
}
