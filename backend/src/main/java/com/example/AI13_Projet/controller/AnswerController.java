package com.example.AI13_Projet.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.example.AI13_Projet.model.Answer;
import com.example.AI13_Projet.repository.AnswerRepository;
import com.example.AI13_Projet.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.AI13_Projet.dto.AnswerDTO;
import com.example.AI13_Projet.exception.ResourceNotFoundException;
import com.example.AI13_Projet.service.QuizService;

@RestController
@RequestMapping("/answers")
public class AnswerController {

    @Autowired
    private QuizService quizService;
    @Autowired
    private QuestionService questionService;

    @Autowired
    private AnswerRepository answerRepository;

    @PostMapping
    public ResponseEntity<?> addAnswer(@RequestBody AnswerDTO answerDTO) {
        try {
            AnswerDTO createdAnswer = quizService.addAnswer(answerDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(Map.of(
                    "status", "success",
                    "message", "Answer created successfully.",
                    "data", createdAnswer
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", "Related resource not found: " + e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred while creating the answer.",
                    "details", e.getMessage()
            ));
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateAnswer(@PathVariable Long id, @RequestBody AnswerDTO answerDTO) {
        try {
            AnswerDTO updatedAnswer = quizService.updateAnswer(id, answerDTO);
            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "Answer updated successfully.",
                    "data", updatedAnswer
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", "Answer not found: " + e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred while updating the answer.",
                    "details", e.getMessage()
            ));
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAnswer(@PathVariable Long id) {
        try {
            quizService.deleteAnswer(id);
            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "Answer deleted successfully."
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", "Answer not found: " + e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred while deleting the answer.",
                    "details", e.getMessage()
            ));
        }
    }

    // Ajouter plusieurs réponses à une question
    @PostMapping("/question/{questionId}")
    public ResponseEntity<?> addAnswersToQuestion(@PathVariable Long questionId, @RequestBody List<AnswerDTO> answersDTO) {
        try {
            List<AnswerDTO> createdAnswers = quizService.addAnswersToQuestion(questionId, answersDTO);

            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "Answers added successfully.",
                    "data", createdAnswers
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", "Question not found: " + e.getMessage()
            ));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Map.of(
                    "status", "error",
                    "message", e.getMessage()
            ));
        }
    }

    @GetMapping("/question/{questionId}")
    public ResponseEntity<?> getAnswersByQuestionId(@PathVariable Long questionId) {
        try {
            List<AnswerDTO> answers = quizService.getAnswersByQuestionId(questionId);

            if (answers.isEmpty()) {
                return ResponseEntity.ok(Map.of(
                        "status", "info",
                        "message", "No answers found for the question with ID " + questionId,
                        "data", answers
                ));
            }

            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "Answers retrieved successfully.",
                    "data", answers
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", "Question not found: " + e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred while retrieving answers.",
                    "details", e.getMessage()
            ));
        }
    }

    @PutMapping("/{answerId}/position")
    public ResponseEntity<?> updateAnswerPosition(@PathVariable Long answerId, @RequestParam int newPosition) {
        try {
            AnswerDTO updatedAnswer = questionService.updateAnswerPosition(answerId, newPosition);
            String message = "Answer position updated to " + newPosition;

            Optional<Answer> swappedAnswer = answerRepository.findByPosition(updatedAnswer.getPosition());
            if (swappedAnswer.isPresent() && !swappedAnswer.get().getId().equals(answerId)) {
                message += ". Position swapped with answer ID: " + swappedAnswer.get().getId();
            }

            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", message,
                    "data", updatedAnswer
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", e.getMessage()
            ));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Map.of(
                    "status", "error",
                    "message", e.getMessage()
            ));
        }
    }

    // Activer ou désactiver une réponse
    @PutMapping("/{id}/status")
    public ResponseEntity<?> updateAnswerStatus(@PathVariable Long id, @RequestParam boolean isActive) {
        try {
            AnswerDTO updatedUser = quizService.updateAnswerStatus(id, isActive);
            String statusMessage = isActive ? "activated" : "deactivated";
            return ResponseEntity.ok(Map.of(
                    "message", "Answer successfully " + statusMessage + ".",
                    "user", updatedUser
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of("error", e.getMessage()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "error", "An unexpected error occurred.",
                    "details", e.getMessage()
            ));
        }
    }
}
