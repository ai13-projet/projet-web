package com.example.AI13_Projet.controller;

import com.example.AI13_Projet.model.Question;
import com.example.AI13_Projet.repository.QuestionRepository;
import com.example.AI13_Projet.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.example.AI13_Projet.dto.QuestionDTO;
import com.example.AI13_Projet.exception.ResourceNotFoundException;
import com.example.AI13_Projet.service.QuizService;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/questions")
public class QuestionController {

    @Autowired
    private QuizService quizService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private QuestionRepository questionRepository;

    @PostMapping
    public ResponseEntity<?> addQuestion(@RequestBody QuestionDTO questionDTO) {
        try {
            // Appel au service pour ajouter la question
            QuestionDTO createdQuestion = quizService.addQuestion(questionDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(Map.of(
                    "status", "success",
                    "message", "Question added successfully.",
                    "data", createdQuestion
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", "Quiz not found: " + e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred while adding the question.",
                    "details", e.getMessage()
            ));
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateQuestion(@PathVariable Long id, @RequestBody QuestionDTO questionDTO) {
        try {
            QuestionDTO updatedQuestion = quizService.updateQuestion(id, questionDTO);
            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "Question updated successfully.",
                    "data", updatedQuestion
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", "Question not found: " + e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred while updating the question.",
                    "details", e.getMessage()
            ));
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteQuestion(@PathVariable Long id) {
        try {
            // Appel au service pour supprimer la question
            quizService.deleteQuestion(id);
            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "Question deleted successfully."
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", "Question not found: " + e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred while deleting the question.",
                    "details", e.getMessage()
            ));
        }
    }

    // Ajouter plusieurs questions à un quiz
    @PostMapping("quiz/{quizId}")
    public ResponseEntity<?> addQuestionsToQuiz(
            @PathVariable Long quizId, @RequestBody List<QuestionDTO> questionsDTO) {
        try {
            List<QuestionDTO> createdQuestions = questionService.addQuestionsToQuiz(quizId, questionsDTO);

            return ResponseEntity.status(HttpStatus.CREATED).body(Map.of(
                    "status", "success",
                    "message", "Questions added successfully.",
                    "data", createdQuestions
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", "Quiz not found: " + e.getMessage()
            ));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Map.of(
                    "status", "error",
                    "message", e.getMessage()
            ));
        }
    }

    // Obtenir toutes les questions d'un quiz
    @GetMapping("quiz/{quizId}")
    public ResponseEntity<?> getQuestionsByQuiz(@PathVariable Long quizId) {
        try {
            List<QuestionDTO> questions = questionService.getQuestionsByQuiz(quizId);
            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", "Questions retrieved successfully.",
                    "data", questions
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", "Quiz not found: " + e.getMessage()
            ));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "status", "error",
                    "message", "An unexpected error occurred.",
                    "details", e.getMessage()
            ));
        }
    }

    @PutMapping("/{questionId}/position")
    public ResponseEntity<?> updateQuestionPosition(@PathVariable Long questionId, @RequestParam int newPosition) {
        try {
            QuestionDTO updatedQuestion = questionService.updateQuestionPosition(questionId, newPosition);
            String message = "Question position updated to " + newPosition;

            Optional<Question> swappedQuestion = questionRepository.findByPosition(updatedQuestion.getPosition());
            if (swappedQuestion.isPresent() && !swappedQuestion.get().getId().equals(questionId)) {
                message += ". Position swapped with question ID: " + swappedQuestion.get().getId();
            }

            return ResponseEntity.ok(Map.of(
                    "status", "success",
                    "message", message,
                    "data", updatedQuestion
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of(
                    "status", "error",
                    "message", e.getMessage()
            ));
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Map.of(
                    "status", "error",
                    "message", e.getMessage()
            ));
        }
    }

    @PutMapping("/{id}/status")
    public ResponseEntity<?> updateQuestionStatus(@PathVariable Long id, @RequestParam boolean isActive) {
        try {
            QuestionDTO updatedQuestion = quizService.updateQuestionStatus(id, isActive);
            String statusMessage = isActive ? "activated" : "deactivated";
            return ResponseEntity.ok(Map.of(
                    "message", "Question successfully " + statusMessage + ".",
                    "question", updatedQuestion
            ));
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of("error", e.getMessage()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(Map.of(
                    "error", "An unexpected error occurred.",
                    "details", e.getMessage()
            ));
        }
    }

}
